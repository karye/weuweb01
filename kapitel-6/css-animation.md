# Labb: CSS-animationer

## Introduktion
CSS-animationer gör att vi kan ge liv till webbsidor genom att animera olika element, t.ex. färger, storlekar och rörelser. I denna labb börjar vi från grunden och lär oss steg för steg hur man skapar enkla animationer.

## Video

{% embed url="https://youtu.be/z2LQYsZhsFw?si=KsrFhfFo-fTYRQl9" %}

## Skapa ett HTML- och CSS-dokument
1. **Skapa en ny HTML-fil** och koppla den till en CSS-fil:
```html
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CSS-animationer</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
    <h1>Välkommen till CSS-animationer!</h1>
    <div class="ruta"></div>
</body>
</html>
```

2. **Skapa en CSS-fil** (styles.css):
```css
body {
    font-family: Arial, sans-serif;
    text-align: center;
    background-color: #f3f4f6;
}

.ruta {
    width: 100px;
    height: 100px;
    background-color: #3498db;
    margin: 50px auto;
}
```

3. **Öppna din HTML-fil i en webbläsare** och bekräfta att en blå ruta syns på sidan.

## Förstå CSS-animationer

1. **Vad är CSS-animationer?**

Animationer i CSS gör att vi kan skapa rörelser och förändringar över tid. Till exempel kan en ruta gradvis ändra färg eller flytta sig från en plats till en annan.

2. **Vad är `@keyframes`?**

* `@keyframes` används för att definiera hur en animation ska förändras över tid. 
* Du anger olika steg för animationen, t.ex. start (`0%`) och slut (`100%`).

3. **Ett exempel på `@keyframes`:**
```css
@keyframes changeColor {
    0% {
        background-color: #3498db; /* Startfärg */
    }
    100% {
        background-color: #e74c3c; /* Slutfärg */
    }
}
```

* Här börjar rutan som blå och slutar som röd.
* Dessa steg används i en CSS-animation för att skapa effekten.

4. **Hur kopplar man en animation till ett element?**
För att använda en `@keyframes`-animation, lägger du till en `animation`-regel:
```css
.ruta {
    animation: changeColor 2s infinite;
}
```

* `changeColor`: Animationens namn (från `@keyframes`).
* `2s`: Hur lång tid animationen ska ta (2 sekunder).
* `infinite`: Animationen upprepas oändligt.

5. **Spara och ladda om sidan.**

* Du bör nu se att rutan gradvis byter färg mellan blå och röd.

## Fler steg i en animation

![alt text](image.png)

1. **Lägg till flera steg i animationen:**
```css
@keyframes changeColorAndSize {
    0% {
        background-color: #3498db;
        transform: scale(1);
    }
    50% {
        background-color: #2ecc71;
        transform: scale(1.5);
    }
    100% {
        background-color: #e74c3c;
        transform: scale(1);
    }
}

.ruta {
    animation: changeColorAndSize 4s infinite;
}
```

2. **Vad händer här?**

* `transform: scale(1)`: Rutan börjar i normal storlek.
* `transform: scale(1.5)`: Vid 50% av animationen blir rutan 1,5 gånger större.
* Slutligen återgår rutan till sin ursprungliga storlek.

### Uppgifter

* **Flera steg:**

* Skapa en animation där en ruta växer och krymper samtidigt som färgen ändras.

* **Utforska:**

* Lägg till fler egenskaper, t.ex. `border-radius` för att göra rutan rund under animationen.

* **Överkurs:**

* Kombinera två olika animationer, t.ex. en som roterar rutan och en som ändrar storlek.
* Tips: Använd tex `transform: rotate(360deg)` för att rotera en ruta ett helt varv.

### Resurser
* [CSS @keyframes](https://developer.mozilla.org/en-US/docs/Web/CSS/@keyframes)
* [CSS Animation Guide](https://www.w3schools.com/css/css3_animations.asp)

Nu när du har lärt dig grunderna är det dags att bygga på med mer avancerade animationer. Vi kommer att utforska hur man skapar komplexa rörelser, använder bilder och emojis, samt hur man kombinerar flera animationer för att skapa imponerande effekter.

## Rörelse och positionering med `translate`
1. **Lägg till en animation för att flytta rutan horisontellt:**
```css
@keyframes moveRight {
    0% {
        transform: translateX(0);
    }
    50% {
        transform: translateX(200px);
    }
    100% {
        transform: translateX(0);
    }
}

.ruta {
    animation: moveRight 3s infinite;
}
```

2. **Förklaring:**

* `translateX(0)`: Börjar på sin ursprungliga position.
* `translateX(200px)`: Flyttar rutan 200 pixlar åt höger.
* Animationen går tillbaka till sin ursprungliga position vid 100%.

3. **Utmaning:**

* Kombinera rörelsen med en färgförändring.

## Bilder och transparens
1. **Lägg till en bild och animera dess opacitet:**
```html
<img src="din-bild.jpg" alt="En bild" class="fade-image">
```

2. **CSS för att animera bilden:**
```css
.fade-image {
    width: 200px;
    animation: fadeInOut 4s infinite;
}

@keyframes fadeInOut {
    0%, 100% {
        opacity: 0; /* Bilden är osynlig */
    }
    50% {
        opacity: 1; /* Bilden är helt synlig */
    }
}
```

3. **Förklaring:**

* `opacity: 0`: Bilden är genomskinlig.
* `opacity: 1`: Bilden är fullt synlig.
* Bilden försvinner och kommer tillbaka i en loop.

## Skapa en studsande emoji

![alt text](../.gitbook/assets/image-124.png)

1. **Lägg till en emoji i HTML:**
```html
<p class="emoji">🌟</p>
```

2. **Skapa en studsande animation:**
```css
.emoji {
    font-size: 50px;
    animation: bounce 2s infinite;
}

@keyframes bounce {
    0%, 100% {
        transform: translateY(0);
    }
    50% {
        transform: translateY(-30px);
    }
}
```

3. **Förklaring:**

* `translateY(0)`: Börjar på sin ursprungliga position.
* `translateY(-30px)`: Hoppar uppåt 30 pixlar.
* Återgår till sin ursprungliga position i slutet.

## Kombinera flera animationer
1. **Skapa en ruta som både snurrar och växer:**
```css
@keyframes rotateAndScale {
    0% {
        transform: rotate(0deg) scale(1);
    }
    50% {
        transform: rotate(180deg) scale(1.5);
    }
    100% {
        transform: rotate(360deg) scale(1);
    }
}

.ruta {
    animation: rotateAndScale 4s infinite;
}
```

2. **Förklaring:**

* `rotate(0deg)`: Startar utan rotation.
* `rotate(180deg)`: Snurrar halvvägs runt vid 50%.
* `scale(1.5)`: Växer samtidigt som den snurrar.
* Slutligen återgår till sin ursprungliga storlek och rotation.

## Tidsstyrda animationer med `animation-delay`
1. **Fördröj en animation:**

```css
.ruta {
    animation: rotateAndScale 4s infinite;
    animation-delay: 2s; /* Startar efter 2 sekunder */
}
```

2. **Skapa flera rutor som startar vid olika tidpunkter:**

```html
<div class="ruta delay-1"></div>
<div class="ruta delay-2"></div>
<div class="ruta delay-3"></div>
```

```css
.delay-1 {
    animation-delay: 1s;
}

.delay-2 {
    animation-delay: 2s;
}

.delay-3 {
    animation-delay: 3s;
}
```

3. **Effekt:**

* Flera element börjar sin animation vid olika tidpunkter.

### Uppgifter

* **Studsande bilder:**

* Använd en bild och skapa en animation där den hoppar upp och ner.

* **Emoji-parad:**

* Skapa flera emojis som animeras med olika effekter och fördröjningar.

* **Ljusspel:**

* Skapa en animation där en ruta gradvis lyser upp och mörknar.

* **Överkurs:**

* Bygg en animation där en text "skriver sig själv" med hjälp av `@keyframes` och `width`.

### Extra utmaningar

#### Skapa en kantanimation

![alt text](../.gitbook/assets/image-119.png)

Här en tutorial på hur man skapar en kantanimation som Google Chromecast använder sig av.

{% embed url="https://youtu.be/ezP4kbOvs_E?si=8D7vvMERzPVtgJ6i" %}

### Resurser

* [CSS Animation Timing Functions](https://developer.mozilla.org/en-US/docs/Web/CSS/animation-timing-function)
* [CSS Transforms](https://developer.mozilla.org/en-US/docs/Web/CSS/transform)
* [Keyframes Advanced Examples](https://css-tricks.com/snippets/css/keyframe-animation-syntax/)