# Labb: Skapa en hamburger-meny

Den här labben lär dig hur du skapar en responsiv hamburger-meny utan extra klasser på navigationsmenyn, med fokus på enkelhet och funktionalitet.

## Steg 1 - Skapa HTML-strukturen

Skapa en ny fil, `index.html`, och lägg in följande kod:

```html
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Hamburger-meny</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <nav>
        <!-- Hamburgerikon -->
        <div class="hamburger">
            <span></span>
            <span></span>
            <span></span>
        </div>
        <!-- Navigationslänkar -->
        <ul>
            <li><a href="#home">Hem</a></li>
            <li><a href="#about">Om oss</a></li>
            <li><a href="#services">Tjänster</a></li>
            <li><a href="#contact">Kontakt</a></li>
        </ul>
    </nav>
</body>
</html>
```

### Förklaring av koden
- `nav`: Innehåller både hamburger-ikonen och menyns länkar.
- `div.hamburger`: Representerar hamburger-ikonen med tre linjer (`span`).
- `ul`: Innehåller navigationslänkarna som visas eller döljs baserat på interaktiviteten.

## Steg 2 - Styla med CSS

Skapa en ny fil, `style.css`, och lägg in följande kod.

### Grundläggande styling

```css
/* Återställ grundinställningar */
* {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
}

/* Basstil för kroppen */
body {
    font-family: Arial, sans-serif;
    line-height: 1.6;
}

/* Navigation */
nav {
    background-color: #333;
    color: white;
    padding: 10px 20px;
    position: relative;
}
```

### Hamburger-ikonen

```css
.hamburger {
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    width: 30px;
    height: 25px;
    cursor: pointer;
}

.hamburger span {
    display: block;
    height: 3px;
    background-color: white;
    border-radius: 5px;
    transition: transform 0.3s ease;
}
```

Förklaring:
- `.hamburger`: Layouten för ikonen, med tre horisontella linjer som staplas vertikalt.
- `.hamburger span`: Varje linje är en rektangel med mjuk övergång för animationer.

### Navigationsmenyn

```css
nav ul {
    list-style: none;
    display: none; /* Dölj menyn som standard */
    flex-direction: column;
    gap: 10px;
    background-color: #444;
    padding: 10px;
    border-radius: 5px;
    position: absolute;
    top: 100%; /* Placera under hamburger-ikonen */
    left: 0;
    right: 0;
}

nav ul li {
    text-align: center;
}

nav ul a {
    text-decoration: none;
    color: white;
    padding: 10px;
    display: block;
}

nav ul a:hover {
    background-color: #555;
    border-radius: 5px;
}
```

Förklaring:
- `nav ul`: Menyn är gömd med `display: none` och visas vid interaktion.
- `position: absolute`: Länkarna placeras under hamburger-ikonen.
- `a:hover`: Ger en visuell effekt när användaren hovrar över en länk.

### Responsiv design

```css
/* Visa menyn när hamburger-ikonen aktiveras */
.hamburger.active + ul {
    display: flex;
}

/* Desktop-layout */
@media (min-width: 768px) {
    .hamburger {
        display: none;
    }
    nav ul {
        display: flex;
        flex-direction: row;
        gap: 20px;
        background: none;
        position: static;
        padding: 0;
    }
}
```

Förklaring:
- `.hamburger.active + ul`: När hamburger-ikonen klickas, växlas menyn till att visas.
- `@media`: På större skärmar döljs hamburger-ikonen och menyn visas horisontellt.

## Steg 3 - Lägg till interaktivitet

Lägg till följande JavaScript i slutet av `index.html`:

```html
<script>
    const hamburger = document.querySelector('.hamburger');
    const menu = document.querySelector('nav ul');

    hamburger.addEventListener('click', () => {
        hamburger.classList.toggle('active');
        menu.classList.toggle('active');
    });
</script>
```

Förklaring:
- JavaScript lyssnar efter klick på hamburger-ikonen.
- Klassen `active` växlas på både hamburger-ikonen och menyn för att visa eller dölja menyn.

## Testa din design

1. Öppna `index.html` i en webbläsare.
2. Klicka på hamburger-ikonen för att visa och dölja menyn.
3. Ändra webbläsarens fönsterstorlek för att testa den responsiva designen.

## Utmaning

1. Lägg till animationer för en smidigare öppning och stängning av menyn.
2. Experimentera med olika layouter och färgkombinationer.
3. Lägg till en bakgrundsskuggning på sidan när menyn är öppen.