---
description: Hur man bygger ett formulär med alla dess delar.
---

# Kontaktformulär

Nu skall vi titta på hur man bygger ett formulär med alla dess delar. 

Ett formulär består av följande delar:

* `form` - hela formuläret 
* `fieldset` - en grupp av relaterade fält
* `legend` - en rubrik till en grupp av fält
* `label` - en etikett till ett fält
* `input` - ett fält där användaren kan skriva in något
* `textarea` - ett större fält där användaren kan skriva in något
* `select` - en lista med val
* `option` - ett val i en lista
* `button` - en knapp

## Resultat

![](<../.gitbook/assets/image (68).png>)

## Video

### Allt om formulär med HTML5

{% embed url="https://youtu.be/f8KpNOAPFpo?si=TfSP1DTa6_B79WDQ" %}

## Steg 1 - förberedelser - webbrot

* Skapa en mapp som heter **formular**
* Skapa en webbsida som heter **kontakt.html**
* Skapa en CSS-fil som heter **kontakt.css**
* Skapa en mapp **bilder**

## Steg 2 - skapa HTML-sidan

### HTML-strukturen

Såhär ser sidans HTML-struktur ut:

* `.kontainer` - en stor box för hela sidan
  * `form` - ett formulär
    * `label` - en etikett till ett fält
    * `input` - ett fält där användaren kan skriva in något
    * ..
    * `textarea` - ett större fält där användaren kan skriva in något
    * `select` - en lista med val
      * `option` - ett val i en lista
    * `button` - en knapp

### Grundkoden

* Börja med grundkoden
* Fyll i alla HTML-element som bygger upp sidan
* Så här ett formulär ut: [https://devdocs.io/html/element/form](https://devdocs.io/html/element/form)

![](../.gitbook/assets/image-63.png)

![](../.gitbook/assets/image-64.png)

## Steg 3 - snygga till sidan med CSS

### CSS-reglerna

* Infoga först CSS-reset
* Infoga alla CSS-regler som motsvarar de taggar vi använder
* **Välj typsnitt**
* Gå till [Google Fonts](https://fonts.google.com) och välj två typsnitt

```css	
/* Enkel CSS-reset */
html {
    box-sizing: border-box;
}
*, *:before, *:after {
    box-sizing: inherit;
}
body, h1, h2, h3, h4, h5, h6, p, ul {
    margin: 0;
    padding: 0;
}
img {
    max-width: 100%;
    height: auto;
}
```

![](../.gitbook/assets/image-65.png)

## Extra uppgift

### Egen stil på radioknappar och kryssrutor

Det går att göra snyggare radioknappar och kryssrutor med CSS. Det finns en hel del kod att lägga till. Det finns också färdiga bibliotek att använda. Här är ett exempel på en snygg radioknapp och kryssruta.

![](<../.gitbook/assets/image (80).png>)

* Prova styla enligt CSS nedan

{% tabs %}
{% tab title="HTML" %}
```html
<div>
  <label>
    <input type="checkbox" class="option-input checkbox" checked />
    Checkbox
  </label>
  <label>
    <input type="checkbox" class="option-input checkbox" />
    Checkbox
  </label>
  <label>
    <input type="checkbox" class="option-input checkbox" />
    Checkbox
  </label>
</div>

<div>
  <label>
    <input type="radio" class="option-input radio" name="example" checked />
    Radio option
  </label>
  <label>
    <input type="radio" class="option-input radio" name="example" />
    Radio option
  </label>
  <label>
    <input type="radio" class="option-input radio" name="example" />
    Radio option
  </label>
</div>
```
{% endtab %}

{% tab title="CSS" %}
```css
@keyframes click-wave {
  0% {
    height: 40px;
    width: 40px;
    opacity: 0.35;
    position: relative;
  }
  100% {
    height: 200px;
    width: 200px;
    margin-left: -80px;
    margin-top: -80px;
    opacity: 0;
  }
}

.option-input {
  appearance: none;
  position: relative;
  top: 13.33333px;
  right: 0;
  bottom: 0;
  left: 0;
  height: 40px;
  width: 40px;
  transition: all 0.15s ease-out 0s;
  background: #cbd1d8;
  border: none;
  color: #fff;
  cursor: pointer;
  display: inline-block;
  margin-right: 0.5em;
  outline: none;
  position: relative;
  z-index: 1000;
}
.option-input:hover {
  background: #9faab7;
}
.option-input:checked {
  background: #40e0d0;
}
.option-input:checked::before {
  height: 40px;
  width: 40px;
  position: absolute;
  content: "✔";
  display: inline-block;
  font-size: 26.66667px;
  text-align: center;
  line-height: 40px;
}
.option-input:checked::after {
  animation: click-wave 0.65s;
  background: #40e0d0;
  content: "";
  display: block;
  position: relative;
  z-index: 100;
}
.option-input.radio {
  border-radius: 50%;
}
.option-input.radio::after {
  border-radius: 50%;
}

body label {
  display: block;
  line-height: 40px;
}
```
{% endtab %}
{% endtabs %}
