---
description: Hur man gör en modern webbsidan.
---

# Modern webb med semantisk HTML

![alt text](../.gitbook/assets/image-130.png)

I den här övningsuppgiften ska vi skapa en webbplats som handlar om [apan som tog selfies](https://sv.wikipedia.org/wiki/Apselfie). Vi ska använda oss **semantisk HTML**. 

**Semantisk HTML** beskriver vad innehållet på webbsidan handlar om. Vi använder oss av taggar som beskriver innehållet, som tex `<header>`, `<main>`, `<aside>`, `<article>`, `<footer>` och `<nav>`.

Det finns flera fördelar med **semantisk HTML**:

* **Semantisk HTML** gör sidan mer tillgänglig för användare med funktionsnedsättningar. Tex kan en skärmläsare läsa upp sidan för en användare som inte kan se. En skärmläsare kan läsa upp sidan på olika sätt beroende på vilka taggar vi använder. En skärmläsare kan tex läsa upp en `<h1>`-tagg som "rubrik 1" och en `<p>`-tagg som "stycke".
* **Semantisk HTML** är också bra för sökmotoroptimering. Sökmotorer som Google kan läsa av sidan och förstå vad den handlar om. Det gör att sidan kan hamna högre upp i sökresultaten.
* **Semantisk HTML** är också bra för att underlätta för andra utvecklare att förstå koden. Om vi använder taggar som beskriver innehållet blir det lättare att förstå vad koden gör.

![alt text](../.gitbook/assets/semantiska-element.png)

## Video 

### Så funkar semantisk HTML

{% embed url="https://youtu.be/TNkeftWSD-4?si=UfD81BfuMMFMSFu5" %}

## Steg 1 - förberedelser - webbrot

* Skapa en mapp som heter **apselfie**
* Skapa en webbsida som heter **index.html**
* Skapa en CSS-fil som heter **style.css**
* Skapa en mapp **bilder**

## Steg 2 - skapa HTML-sida


```mermaid
block-beta
  columns 2
  header:2
  main aside
  footer:2
```

### HTML-strukturen

Såhär ser sidans HTML-struktur ut:

* `.kontainer` - en stor box för hela sidan
  * `header` - sidans sidhuvud
    * `h1` - sidans rubrik
    * `h2` - sidans underrubrik
    * `nav` - sidans navigering
      * `a` - en länk
  * `main` - sidans huvudinnehåll
    * `article` - en artikel
      * `h3` - artikelns mellanrubrik
      * `p` - artikelns text
      * `img` - artikelns bild
  * `aside` - sidans sidofält
    * `h3` - sidofältets mellanrubrik
    * `p` - sidofältets text
  * `footer` - sidans sidfot
    * `p` - sidfotens text

### Grundkoden

* Börja med grundkoden
* Fyll i alla HTML-element som bygger upp sidan från wiki-sidan om [apselfie](https://sv.wikipedia.org/wiki/Apselfie)
* Bilden på apan kan du ladda ner från [Wikipedia](http://upload.wikimedia.org/wikipedia/commons/3/36/FRIM_canopy.JPG)


![alt text](../.gitbook/assets/image-131.png)

## Steg 3 - snygga till sidan med CSS

### CSS-reglerna

* Infoga först CSS-reset
* Infoga alla CSS-regler som motsvarar de taggar vi använder
* Gå till [Google Fonts](https://fonts.google.com) och välj två typsnitt

```css	
/* Enkel CSS-reset */
html {
    box-sizing: border-box;
}
*, *:before, *:after {
    box-sizing: inherit;
}
body, h1, h2, h3, h4, h5, h6, p, ul {
    margin: 0;
    padding: 0;
}
img {
	max-width: 100%;
	height: auto;
}
```

### Planera layouten med grid-area

Med `grid-area` namnger vi de olika delarna av sidan. Vi kan sedan använda namnen för att placera ut elementen på sidan.

![](<../.gitbook/assets/image (87).png>)

Sedan använder vi `grid-template-areas` för att placera ut elementen på sidan såhär:

```text
sidhuvud sidhuvud
meny meny
huvudinnehåll sidebar
sidfot sidfot
```

Detta motsvarar en layout som ser ut såhär:

| sidhuvud | sidhuvud |
| --- | --- |
| meny | meny |
| huvudinnehåll | sidebar |
| sidfot | sidfot |

![alt text](../.gitbook/assets/image-132.png)

### Allmäna stylingen

![alt text](../.gitbook/assets/image-137.png)

### Sidhuvudet

![alt text](../.gitbook/assets/image-136.png)

### Menyn 

![alt text](../.gitbook/assets/image-133.png)

### Sidans innhåll

![alt text](../.gitbook/assets/image-134.png)

### Sidebar och sidfoten

![alt text](../.gitbook/assets/image-135.png)