# Klassiska layouter (del 1)

## Översikt: Semantiska element

**Semantiska element** är HTML‑taggar som inte bara strukturerar innehållet utan även beskriver dess innebörd. Exempelvis:

- **`<header>`**: Innehåller sidans eller en sektions introduktion (t.ex. rubrik, logotyp).
- **`<nav>`**: Innehåller navigationslänkar.
- **`<main>`**: Innehåller det huvudsakliga, unika innehållet på sidan (endast ett per sida, och det ska inte ligga inuti t.ex. `<header>`, `<nav>`, `<aside>`, `<footer>` eller `<article>`).
- **`<article>`**: Markerar en självständig innehållsenhet, exempelvis en bloggartikel eller ett nyhetsinlägg.
- **`<aside>`**: Innehåller kompletterande information eller sidoinnehåll (t.ex. sidofält med extra länkar eller annonser).
- **`<footer>`**: Innehåller sidfotens innehåll, ofta återkommande över sidor (t.ex. copyright‑information).

Att använda dessa element har flera fördelar:

- **Tillgänglighet**: Hjälpmedel som skärmläsare kan enklare navigera och hoppa till huvudinnehållet.
- **SEO**: Sökmotorer kan bättre förstå sidstrukturen.
- **Läsbarhet och underhåll**: Koden blir mer självdokumenterande vilket är bra för både elever och utvecklare.

En viktig regel är att **endast ett `<main>`-element får finnas per dokument** och att det omsluter det innehåll som är unikt för just den sidan.

## Exempel 1: Klassisk layout med header, nav, main, aside och footer

```mermaid
block-beta
  columns 2
  header:2
  nav:2
  main aside
  footer:2
```

I detta exempel sträcker sig `header`, `nav` och `footer` över hela bredden, medan huvudinnehållet (main) placeras bredvid ett sidofält (`aside`) i en tvåkolumnslayout.

### HTML

```html
<div class="kontainer">
  <header>Portfolio</header>
  <nav>Meny</nav>
  <main>Huvudinnehåll</main>
  <aside>Sidofält</aside>
  <footer></footer>
</div>
```

### CSS

CSS‑reglerna definierar layouten med hjälp av `grid-template-areas` och `grid-template-columns`. Varje element får en egen `grid-area` som matchar namnet i HTML‑koden.

```css
/* Klassisk layout med header, nav, main, aside och footer */
.kontainer {
  display: grid;
  grid-template-areas:
    "header header"
    "nav nav"
    "main aside"
    "footer footer";
  grid-template-columns: 2fr 1fr;
  gap: 1em;
  padding: 1em;
}

header {
  grid-area: header;
  background: #f4f4f4;
  padding: 1em;
}

nav {
  grid-area: nav;
  background: #ddd;
  padding: 1em;
}

main {
  grid-area: main;
  background: #eee;
  padding: 1em;
}

aside {
  grid-area: aside;
  background: #ccc;
  padding: 1em;
}

footer {
  grid-area: footer;
  background: #bbb;
  padding: 1em;
  text-align: center;
}
```

## Exempel 2: Kolumnlayout med två kolumner

```mermaid
block-beta
  columns 2
  header:2
  main sidebar
  footer:2
```

Här byggs en enkel tvåkolumnslayout som fyller hela skärmen. `main` innehåller huvudinnehållet och `sidebar` kan användas för kompletterande information eller navigering. Båda kolumnerna har en fast bredd eller en andel av skärmbredden.

### HTML

```html
<div class="kolumnkontainer">
  <header>Header</header>
  <main>Main</main>
  <aside>Sidebar</aside>
  <footer>Footer</footer>
</div>
```

### CSS

```css
.kolumnkontainer {
  display: grid;
  grid-template-areas:
    "header header"
    "main sidebar"
    "footer footer";
  grid-template-columns: 3fr 1fr;
  gap: 1em;
  padding: 1em;
}

header {
  grid-area: header;
  background: #f0f0f0;
  padding: 1em;
}

main {
  grid-area: main;
  background: #fafafa;
  padding: 1em;
  height: 100vh; /* Fyller resten av skärmen */
}

aside {
  grid-area: sidebar;
  background: #e0e0e0;
  padding: 1em;
  height: 100vh; /* Fyller resten av skärmen */
}

footer {
  grid-area: footer;
  background: #d0d0d0;
  padding: 1em;
  text-align: center;
}
```

## Exempel 3: Holy Grail-layout (tre kolumner)

```mermaid
block-beta
  columns 3
  header:3
  sidebar1 main sidebar2
  footer:3
```

Den s.k. **Holy Grail**-layouten är ett klassiskt exempel med tre kolumner där `header` och `footer` sträcker sig över hela bredden. Mittkolumnen (`main`) innehåller huvudinnehållet och de två sidokolumnerna (`sidebar`) erbjuder kompletterande information eller navigering. Layouten kan även anpassas responsivt med media queries för att stapla elementen på mindre skärmar.

### HTML

```html
<div class="kontainer">
  <header>Header</header>
  <aside class="left-sidebar">Left sidebar</aside>
  <main>Main content</main>
  <aside class="right-sidebar">Right sidebar</aside>
  <footer>Footer</footer>
</div>
```

### CSS

```css
.kontainer {
  display: grid;
  grid-template-areas:
    "header header header"
    "left main right"
    "footer footer footer";
  grid-template-columns: 1fr 3fr 1fr;
  gap: 1em;
  padding: 1em;
}
header {
  grid-area: header;
  background: #eaeaea;
  padding: 1em;
}
.left-sidebar {
  grid-area: left;
  background: #dcdcdc;
  padding: 1em;
}
main {
  grid-area: main;
  background: #f7f7f7;
  padding: 1em;
}
.right-sidebar {
  grid-area: right;
  background: #dcdcdc;
  padding: 1em;
}
footer {
  grid-area: footer;
  background: #cfcfcf;
  padding: 1em;
  text-align: center;
}

/* Responsiv anpassning: på mindre skärmar staplas elementen */
@media (max-width: 768px) {
  .holygrail {
    grid-template-areas:
      "header"
      "left"
      "main"
      "right"
      "footer";
    grid-template-columns: 1fr;
  }
}
```

## Sammanfattning

Genom att använda `grid-template-areas` kan du snabbt definiera och visualisera hur de semantiska delarna på din webbsida ska placeras. Skisserna ovan visar hur du kan tänka när du skapar:

- En klassisk layout med en bred header, nav, två kolumner (`main` + `aside`) och en bred footer.
- En enkel tvåkolumnslayout med ett tydligt sidoinnehåll.
- En **Holy Grail**-layout med tre kolumner som är mycket användbar för mer komplexa sidstrukturer.

Dessa exempel är bara en utgångspunkt och kan enkelt utvidgas med fler rader, kolumner och anpassningar via media queries för att uppnå responsiv design. 