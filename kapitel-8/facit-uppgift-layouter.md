# Facit: Uppgift - Layouter med CSS Grid

Nedan följer facit (lösningsförslag) för de tre uppgifterna tillsammans med några extra utmaningar som kan få eleverna att tänka lite extra.

## Uppgift 1: Hero-sektion med två kolumner

### HTML-struktur

```html
<div class="kontainer">
  <header class="hero">Hero</header>
  <main class="innehall">
    <section class="kolumn1">Innehåll kolumn 1</section>
    <section class="kolumn2">Innehåll kolumn 2</section>
  </main>
  <footer class="footer">Footer</footer>
</div>
```

### CSS

```css
/* Även om vi nu delar upp sidan i tre grid-områden (header, main, footer),
   definierar vi först .kontainer med dessa områden */
.kontainer {
  display: grid;
  grid-template-areas:
    "hero hero"
    "innehall innehall"
    "footer footer";
  grid-template-columns: 1fr 1fr;
  gap: 1em;
  padding: 1em;
}
.hero {
  grid-area: hero;
  background: #f4f4f4;
  padding: 1em;
}
/* main får sin egen grid-layout för de två kolumnerna */
.innehall {
  grid-area: innehall;
  display: grid;
  grid-template-areas: "kolumn1 kolumn2";
  grid-template-columns: 1fr 1fr;
  gap: 1em;
  padding: 1em;
}
.kolumn1 {
  grid-area: kolumn1;
  background: #e0e0e0;
  padding: 1em;
}
.kolumn2 {
  grid-area: kolumn2;
  background: #d0d0d0;
  padding: 1em;
}
.footer {
  grid-area: footer;
  background: #c0c0c0;
  padding: 1em;
  text-align: center;
}
```

### Extra utmaningar

1. **Variera kolumnbredder:** Ändra `grid-template-columns` så att den högra kolumnen är dubbelt så bred som den vänstra, t.ex. `grid-template-columns: 1fr 2fr;`.
2. **Responsiv design:** Använd en media query för att stapla elementen (hero, kolumn1, kolumn2 och footer) på en kolumn på smalare skärmar.
3. **Bakgrundsanimation:** Lägg till en enkel `hover`-effekt på hero-sektionen, t.ex. genom att ändra bakgrundsfärgen vid muspekaren.

## Uppgift 2: Magasinlayout med utstickande Featured-artikel

### HTML-struktur

```html
<div class="magasin">
  <header>Header</header>
  <nav>Nav</nav>
  <main>
    <article class="featured">Featured Article</article>
    <article class="artikel1">Artikel 1</article>
    <article class="artikel2">Artikel 2</article>
    <article class="artikel3">Artikel 3</article>
  </main>
  <footer>Footer</footer>
</div>
```

### CSS

```css
/* Yttre grid för magasin-sidan med fyra rader */
.magasin {
  display: grid;
  grid-template-areas:
    "header header header"
    "nav nav nav"
    "main main main"
    "footer footer footer";
  grid-template-columns: repeat(3, 1fr);
  gap: 1em;
  padding: 1em;
}
header {
  grid-area: header;
  background: #eaeaea;
  padding: 1em;
}
nav {
  grid-area: nav;
  background: #dcdcdc;
  padding: 1em;
}

/* main definierar en intern grid-layout för artiklarna */
main {
  grid-area: main;
  display: grid;
  grid-template-areas:
    "featured artikel1 artikel2"
    "artikel3 artikel3 artikel3";
  grid-template-columns: repeat(3, 1fr);
  gap: 1em;
  padding: 1em;
}
.featured {
  grid-area: featured;
  background: #f7f7f7;
  padding: 1em;
}
.artikel1 {
  grid-area: artikel1;
  background: #f0f0f0;
  padding: 1em;
}
.artikel2 {
  grid-area: artikel2;
  background: #f0f0f0;
  padding: 1em;
}
.artikel3 {
  grid-area: artikel3;
  background: #e0e0e0;
  padding: 1em;
}
footer {
  grid-area: footer;
  background: #d0d0d0;
  padding: 1em;
  text-align: center;
}
```

### Extra utmaningar

1. **Ändra placeringen:** Låt featured-artikeln spänna över två rader istället för en, vilket innebär att du behöver anpassa `grid-template-areas` och möjligen öka antalet rader.
2. **Olika kolumnstorlekar:** Prova att sätta `grid-template-columns` till t.ex. `1fr 2fr 1fr` för att ge extra utrymme åt artiklarna.
3. **Interaktiv navigation:** Lägg till en `hover`-effekt på `nav`-elementet och ge det en annan bakgrundsfärg vid muspekning.

## Uppgift 3: Dashboard med widgets i rutnät

### HTML-struktur:**

```html
<div class="dashboard">
  <header>Dashboard Header</header>
  <main class="widget-container">
    <div class="widget widget1">Widget 1</div>
    <div class="widget widget2">Widget 2</div>
    <div class="widget widget3">Widget 3</div>
    <div class="widget widget4">Widget 4</div>
    <div class="widget widget5">Widget 5</div>
    <div class="widget widget6">Widget 6</div>
  </main>
  <footer>Dashboard Footer</footer>
</div>
```

### CSS

```css
/* Yttre grid för dashboard med tre rader */
.dashboard {
  display: grid;
  grid-template-areas:
    "header header header"
    "main main main"
    "footer footer footer";
  grid-template-columns: repeat(3, 1fr);
  gap: 1em;
  padding: 1em;
}
header {
  grid-area: header;
  background: #ececec;
  padding: 1em;
}

/* main-widget-container definierar en intern grid-layout för widgetarna */
.widget-container {
  grid-area: main;
  display: grid;
  grid-template-areas:
    "widget1 widget2 widget3"
    "widget4 widget5 widget6";
  grid-template-columns: repeat(3, 1fr);
  gap: 1em;
  padding: 1em;
}
.widget1 {
  grid-area: widget1;
  background: #f5f5f5;
  padding: 1em;
}
.widget2 {
  grid-area: widget2;
  background: #f5f5f5;
  padding: 1em;
}
.widget3 {
  grid-area: widget3;
  background: #f5f5f5;
  padding: 1em;
}
.widget4 {
  grid-area: widget4;
  background: #e5e5e5;
  padding: 1em;
}
.widget5 {
  grid-area: widget5;
  background: #e5e5e5;
  padding: 1em;
}
.widget6 {
  grid-area: widget6;
  background: #e5e5e5;
  padding: 1em;
}
footer {
  grid-area: footer;
  background: #dcdcdc;
  padding: 1em;
  text-align: center;
}
```

### Extra utmaningar

1. **Övergripande widget:** Lägg till en widget som spänner över två kolumner (t.ex. placera en "Totalöversikt" mellan widget2 och widget3) genom att ändra `grid-template-areas` i den aktuella raden.
2. **Responsiv omställning:** Använd media queries så att dashboarden på små skärmar staplar widgets under varandra (t.ex. en kolumn istället för tre).
3. **Interaktivitet:** Inför en enkel hover-effekt på widgetarna (t.ex. en skugga eller färgändring) för att ge användaren visuell feedback.

### Sammanfattning och allmänna utmaningar

Alla facit visar tydligt hur `grid-template-areas` kan användas för att definiera strukturen i layouten. Några generella utmaningar som kan appliceras på alla uppgifter:

- **Experimentera med gap och padding:** Hur påverkar förändrade värden layoutens utseende?
- **Använd responsiva enheter:** Ersätt fasta enheter med `procent`, `rem` eller `fr` för att se hur layouten anpassar sig till olika skärmstorlekar.
- **Implementera fallback-lösningar:** Hur kan du anpassa layouten om en äldre webbläsare inte stödjer CSS Grid?

Genom att arbeta igenom facit och försöka implementera de extra utmaningarna får eleverna en djupare förståelse för hur CSS Grid fungerar och hur flexibel layoutdesign kan skapas med `grid-template-areas`.