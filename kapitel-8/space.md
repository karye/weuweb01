---
description: En webbplats med bakgrundsvideo och sticky meny
---

# Space Tourism

I den här övningsuppgiften ska vi skapa en webbplats som handlar om rymdturism. Vi ska använda oss av en bakgrundsvideo och en **sticky** meny. 

Vi använder oss av flera webbtekniker:

* **Semantisk HTML** - för att beskriva vad innehållet på webbsidan handlar om.
* CSS `grid` - för att skapa en layout.
* CSS `position: sticky` - för att skapa en fäst i toppen på sidan, även när man skrollar nedåt.
* **Bakgrundsvideo** - för att skapa en video som spelas upp i bakgrunden på sidan.

## Resultat

![](<../.gitbook/assets/image (67).png>)

Se [Webbplatsen](https://karye-webb-1.github.io/wanderer/).

## Video 

### Så funkar semantisk HTML

{% embed url="https://youtu.be/TNkeftWSD-4?si=UfD81BfuMMFMSFu5" %}

## Steg 1 - förberedelser - webbrot

* Skapa en mapp som heter **space-tourism**
* Skapa en webbsida som heter **index.html**
* Skapa en CSS-fil som heter **style.css**
* Skapa en mapp **bilder**

## Steg 2 - skapa HTML-sidan

```mermaid
block-beta
  columns 1
  header
  main
  footer
```

### Första sidan index.html

Bilder och texter hämtas från NASA:s webbplats. Vi använder en bakgrundsvideo som heter "Wanderer" som laddas ned från Vimeo. 

* Exoplanet Travel Bureau: [https://exoplanets.nasa.gov/alien-worlds/exoplanet-travel-bureau](https://exoplanets.nasa.gov/alien-worlds/exoplanet-travel-bureau)
* Hämta bilder och texter på [https://www.jpl.nasa.gov/visions-of-the-future/](https://www.jpl.nasa.gov/visions-of-the-future/)
* Ikoner på [https://tympanus.net/codrops/2016/03/18/freebie-astronomy-space-icons/](https://tympanus.net/codrops/2016/03/18/freebie-astronomy-space-icons/)

![](<../.gitbook/assets/image (97).png>)

#### Vimeo iframe-koden

[**Wanderers - a short film by Erik Wernquist**](https://vimeo.com/108650530) är en kortfilm på Vimeo.

För att bädda in en video från Vimeo använder vi en `<iframe>`-tagg. Här är koden för att bädda in videon "Wanderers - a short film by Erik Wernquist":

```html
<iframe src="https://player.vimeo.com/video/108650530?background=1&autoplay=1&loop=1&byline=0&title=0" 
title="Wanderers - a short film by Erik Wernquist"></iframe>
```

#### Carl Sagan-citatet

Vi använder ett citat av Carl Sagan som blockcitat. Här är citatet:

```text
For all its material advantages, the sedentary life has left us edgy, unfulfilled. 
Even after 400 generations in villages and cities, we haven’t forgotten. 
The open road still softly calls, like a nearly forgotten song of childhood.
```

### Övriga sidor

### HTML-strukturen

Såhär ser sidans HTML-struktur ut:

* `.wanderer` - en stor box för hela sidan
  * `header` - en box för sidhuvudet
    * `h1` - en box för rubriken
    * `nav` - en box för menyn
      * `ul` - en box för listan med länkar
        * `li` - en box för varje länk
            * `a` - länk till annan sida
    * `main` - en box för huvudinnehållet
      * `h2` - en mellanrubrik
      * `figure` - en box för en bild
        * `img` - en bild
        * `figcaption` - en bildtext
      * `p` - en text
      * ...
    * `footer` - en box för sidfoten
        * `p` - texten i sidfoten

![](<../.gitbook/assets/image (99).png>)

## Steg 3 - snygga till sidan med CSS

### CSS-reglerna

* Infoga först CSS-reset
* Infoga alla CSS-regler som motsvarar de taggar vi använder
* **Välj typsnitt**
* Gå till [Google Fonts](https://fonts.google.com/share?selection.family=Fugaz+One|Roboto:ital,wght@0,100..900;1,100..900) och välj typsnittet **Fugaz One** och **Roboto**.

```css	
/* Enkel CSS-reset */
html {
    box-sizing: border-box;
}
*, *:before, *:after {
    box-sizing: inherit;
}
body, h1, h2, h3, h4, h5, h6, p, ul {
    margin: 0;
    padding: 0;
}
img {
    max-width: 100%;
    height: auto;
}
```

### Vanliga CSS-reglerna i **style.css**

De CSS-regler som används på alla sidor lägger vi i **style.css**.

![alt text](../.gitbook/assets/image-153.png)

### Speciella CSS-regler i **special.css**

De CSS-regler som bara används på en sida lägger vi i **special.css**.

1. Exempel på detta är CSS-regler för att skapa en bakgrundsvideo och vertikalt centrerad text.

![alt text](../.gitbook/assets/image-151.png)

2. På alla sidor lägger vi till en sticky footer:

![alt text](../.gitbook/assets/image-152.png)
