---
description: Träna på margin, padding och width
---

# Fijis reklamsida

I den här övningen tränar vi på box-modellen och bakgrundsbilder. Vi använder en bild från en film som bakgrundsbild och placerar texten med `margin` och `padding`.

## Resultat

![](../.gitbook/assets/image-43.png)

## Video

### How the CSS Box Model Works in 2 Minutes

{% embed url="https://youtu.be/YCsp1nATc2o?si=ThZXlD0mVz54QFRC" %}

![](../.gitbook/assets/image-79.png)

## Steg 1 - förberedelser - webbrot

* Skapa en mapp som heter **fiji**
* Skapa en webbsida som heter **fiji.html**
* Skapa en CSS-fil som heter **style.css**
* Skapa en mapp **bilder**

## Steg 2 - skapa HTML-sidan

### HTML-strukturen

Såhär ser sidans HTML-struktur ut:

* `h1` - en rubrik
* `h2` - en mellanrubrik
* `p`
  * `a` - en länk

* Börja med grundkoden
* Fyll i alla HTML-element som bygger upp sidan

![](../.gitbook/assets/image-34.png)

## Steg 3 - styla sidan med CSS

### CSS-reglerna

* Länka till CSS-filen
* Infoga alla CSS-regler som motsvarar de taggar vi använder

![](../.gitbook/assets/image-35.png)

### Välj ett sans-serif typsnitt

* Gå till [Google Fonts](https://fonts.google.com) och välj ett **sans-serif** typsnitt
* Ställ in texternas storlek med `font-size`

![](../.gitbook/assets/image-36.png)

### Ställ in bakgrundsbilden

* Välj ut ett foto på [unsplash.com](https://unsplash.com)\
  Välj en bild som är minst 2000px bred
* Ladda ned fotot i mappen `bilder`
* Använd `background-image` på `body`-elementet

![](../.gitbook/assets/image-37.png)

### Anpassa mellanrum

* Använd `margin` på elementen

### Elementet h1
* Ställ in `margin-top: 100px;`
* Ställ in `margin-left: 1500px;`

![](../.gitbook/assets/image-66.png)

### Elementet h2
* Ställ in `margin-left: 150px;`

![](../.gitbook/assets/image-67.png)

### Elementet p
* Ställ in `margin-left: 150px;`

![](../.gitbook/assets/image-68.png)

#### style.css
* Sammantaget blir det såhär i CSS-filen:

![](../.gitbook/assets/image-38.png)

### Styla länken som en knapp

* Länken ligger i taggen `<p>`
* Använd `padding`, `border` och `background-color` för att styla som en knapp

### Andra texteffekter
* Använd `text-transform` för att göra text till versaler
* Testa även `text-shadow` för att få en snygg effekt

![](../.gitbook/assets/image-62.png)

## Uppgift

* Skapa en webbsida om en produkt du gillar
* Använd en bakgrundsbild
* Använd taggar som `h1`, `h2`, `p` och `a`
* Använd `margin` och `padding` för att placera texten