---
description: Träna på formatering och styla med CSS
---

# Personporträtt - skapa en sida om en känd person

I den här uppgiften ska du skapa en enkel och snygg webbsida som presenterar en person du beundrar eller är intresserad av. 

![alt text](../.gitbook/assets/image-113.png)

## Instruktioner

1. **Välj en person att presentera:**  
   Personen kan vara en kändis, en historisk figur, en idrottare eller någon annan du ser upp till.

2. **Hämta bilder och text:**
   - **Bilder:** Gå till [unsplash.com](https://unsplash.com) eller en annan bildkälla och ladda ner minst en bild som representerar personen. Lägg bilden i en mapp som heter **bilder**.
   - **Text:** Besök [Wikipedia](https://www.wikipedia.org) och sök efter din person. Kopiera ett kort stycke (några meningar) som beskriver personen och klistra in det i din webbsida.
   - **Symboler:** Lägg till symboler från [HTML Entities](https://www.toptal.com/designers/htmlarrows/).

3. **Skapa HTML och CSS:**
   - Skapa en mapp för projektet och döp den till **portratt**.
   - I mappen, skapa en HTML-fil *index.html* och en CSS-fil *style.css*.
   - Bygg upp sidan med hjälp av följande element:
     - 1 huvudrubrik `h1` med personens namn.
     - 3-5 stycken text `p` som beskriver personen, personens födelsedatum eller annan viktig information.
     - 1-3 bilder `img` som illustrerar personen.
     - `a` för att länka.

4. **Använd CSS-regler:**
   - Använd CSS för att styla sidan.
   - Anpassa layouten med `margin`, `padding`, `border` och `background`.
   - Välj ett typsnitt från [Google Fonts](https://fonts.google.com).
   - Styla texten med `font`, `color`, `text-shadow`, och andra egenskaper för att göra sidan mer intressant.

5. **Anpassa designen:**
   - Testa olika färger, marginaler och mellanrum för att få en harmonisk layout.
   - Använd olika bildeffekter som `border-radius` eller `box-shadow` för att ge sidan ett professionellt utseende.

6. **Ange källa:**
   - Glöm inte att ange källan till din text från Wikipedia. Lägg till ett stycke längst ned som säger "Källa: Wikipedia".

## Exempel på struktur

- **Huvudrubrik:** Personens namn, t.ex. "Marie Curie"
- **Text:** En beskrivning av personen, t.ex. "Marie Curie var en polsk-fransk fysiker och kemist..."
- **Bilder:** En eller flera bilder som representerar personen.

## CSS-egenskaper att använda
- **Layout:** Använd `margin`, `padding` och `border` för att skapa mellanrum och struktur.
- **Bakgrund:** Lägg till en bakgrundsfärg eller bakgrundsbild.
- **Text:** Styla rubriker och stycken med `font-family`, `color` och `text-transform`.
- **Bilder:** Styla bilden med `border-radius` för runda hörn eller `box-shadow` för en skuggeffekt.
