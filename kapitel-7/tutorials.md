# Youtube grid-videor

När du vill fördjupa dig i CSS grid kan du titta igenom dessa tutorials. De är på engelska.

## CSS Grid Layout Crash Course

{% embed url="https://youtu.be/jV8B24rSN5o" %}

## 10 modern layouts in 1 line of CSS

{% embed url="https://youtu.be/qm0IfG1GyZU" %}

## Easily Structure your Layout with CSS Grid's 'grid-template-areas'

{% embed url="https://youtu.be/qTGbWfEEnKI" %}
