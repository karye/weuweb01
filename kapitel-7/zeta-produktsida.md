---
description: Med CSS grid är det lätt att skapa rutor.
---

# Uppgift - Zeta produktsida

[https://www.zeta.nu/produkter](https://www.zeta.nu/produkter/) är hemsidan som vi ska efterlikna.

Det är vanligt att webbsidor är uppbyggda av rutor som ligger bredvid varandra. Med CSS `grid` är det lätt att skapa rutor som ligger bredvid varandra.

Menyn kan byggas `display: flex;` för att skapa en horisontell meny.\
Det inre rutnätet med produkter skapas med `display: grid;` och för att skapa fyra kolumner med lika stor bredd. 

## Resultat

![alt text](../.gitbook/assets/image-126.png)

## Video

### Learn CSS Grid the easy way

{% embed url="https://youtu.be/rg7Fvvl3taU?si=XIc-sTHAOM3_CEh5" %}

Testa även [CSS Grid Garden](https://cssgridgarden.com/#sv) för att lära dig CSS grid.

## Steg 1 - förberedelser - webbrot

* Skapa en mapp som heter **zeta**
* Skapa en webbsida som heter **produkter.html**
* Skapa en CSS-fil som heter **style.css**
* Skapa en mapp **bilder**

### Bildmaterial

![alt text](../.gitbook/assets/site-logo.png)

![alt text](../.gitbook/assets/Zeta_middag.jpg)

![alt text](../.gitbook/assets/Gronsaksroror-1-1024x1024.jpg)

![alt text](../.gitbook/assets/Breoliv-1024x1024.jpg)

![alt text](../.gitbook/assets/Olivolja-1024x1024.jpg)

![alt text](../.gitbook/assets/Alla-Zetas-produkter-1024x1024.jpg)

### Färgerna

Det finns två bakgrundsfärger som används på sidan:

* '#F3F0DD' - en ljus gul färg 
<div style="border:1px solid #AAA;background-color: #F3F0DD; width: 100px; height: 100px;"></div>

* '#F9F8F1' - en ljus beige färg
<div style="border:1px solid #AAA;background-color: #F9F8F1; width: 100px; height: 100px;"></div>

## Steg 2 - skapa HTML-sidan 

### Grundkoden

* Börja med grundkoden
* Fyll i alla HTML-element som bygger upp sidan

### HTML-strukturen

Såhär ser sidans HTML-struktur ut:

* `.kontainer` - en stor box för hela sidan - orange
  * `.meny` - en box för sidhuvudet - gul
  * `.sida` - en box för sidinnehållet - röd
    * `.rutor` - en box för alla produkter - grön
      * `.produkt` - en box för varje produkt - blå

## Steg 3 - snygga till sidan med CSS

### CSS-reglerna

* Infoga först CSS-reset
* Infoga alla CSS-regler som motsvarar de taggar vi använder
* Gå till [Google Fonts](https://fonts.google.com) och välj liknande typsnitt

```css	
/* Enkel CSS-reset */
html {
    box-sizing: border-box;
}
*, *:before, *:after {
    box-sizing: inherit;
}
body, h1, h2, h3, h4, h5, h6, p, ul {
    margin: 0;
    padding: 0;
}
img {
    max-width: 100%;
    height: auto;
}
```

### Menyn

Menyn byggs med en div-box och `a` och `img` element.
Stylas med:

* `display: flex;` för att skapa en horisontell meny
* `justify-content: space-between;` för att placera elementen i varsin ände
* `align-items: center;` för att centrera innehållet vertikalt
* `gap` för att skapa mellanrum mellan elementen

### Produkterna

Produkterna byggs med en div-box och `img`, `h2` element.
Stylas med:

* `display: grid;` för att skapa ett rutnät
* `grid-template-columns: 1fr 1fr 1fr 1fr;` för att skapa fyra kolumner med lika stor bredd
* `gap` för att skapa mellanrum mellan rutorna