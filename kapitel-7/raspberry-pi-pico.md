---
description: Med CSS grid är det lätt att skapa boxar i kolumner.
---

# Raspberry Pi webbsida

Nu ska vi titta på hur man skapar en autentisk webbsida. Vi fokuserar på en liten del av sidan, en tre-kolumns layout med produkter. [Raspberry Pi](https://www.raspberrypi.com/).
Vi använder oss av CSS `grid` för att skapa en layout med tre kolumner.

CSS `grid` används ofta för att skapa ett rutnät med kolumner och rader. Det är ett bra sätt att skapa en layout med flera boxar som ligger bredvid varandra:

* `div.kontainer` - en stor box som innehåller alla andra boxar med
  * `display: grid;` - gör att boxarna ligger bredvid varandra
  * `grid-template-columns: 1fr 1fr 1fr;` - ställer in tex tre delar lika stora
  * `grid-gap: 20px;` - ställer in mellanrummet mellan boxarna
* `div.boxar` - flera mindre boxar som ligger bredvid varandra 

## Resultat

![alt text](../.gitbook/assets/image-125.png)

## Video

### The EASIEST way to get started with CSS GRID

{% embed url="https://youtu.be/_lEkD8IGkwo?si=0iW2S2eahFe1WnAt" %}

## Spelet CSS Grid Garden

Testa även [CSS Grid Garden](https://cssgridgarden.com/#sv) för att lära dig CSS grid.

## Steg 1 - förberedelser - webbrot

* Skapa en mapp som heter **raspberry-pi**
* Skapa en webbsida som heter **index.html**
* Skapa en CSS-fil som heter **style.css**
* Skapa en mapp **bilder**

## Steg 2 - skapa HTML-sidan

### HTML-strukturen

Såhär ser sidans HTML-struktur ut:

* `.kontainer` - en stor box för hela sidan
  * `h2` - en rubrik
  * `p` - en beskrivning
  * `.boxar` - en box för alla produkter
    * `.box` - en box för varje produkt
      * `img` - en bild på produkten
      * `h3` - en rubrik på produkten
      * `p` - en beskrivning av produkten

### Grundkoden

* Börja med grundkoden
* Fyll i alla HTML-element som bygger upp sidan
* Hämta material från [https://www.raspberrypi.com/for-home/](https://www.raspberrypi.com/for-home)

![Alt text](../.gitbook/assets/image-14.png)

## Steg 3 - snygga till sidan med CSS

### CSS-reglerna

* Infoga först CSS-reset
* Infoga alla CSS-regler som motsvarar de taggar vi använder
* Gå till [Google Fonts](https://fonts.google.com) och välj liknande typsnitt

```css	
/* Enkel CSS-reset */
html {
    box-sizing: border-box;
}
*, *:before, *:after {
    box-sizing: inherit;
}
body, h1, h2, h3, h4, h5, h6, p, ul {
    margin: 0;
    padding: 0;
}
img {
    max-width: 100%;
    height: auto;
}
```

Först bygger vi hela layouten med en `div.kontainer` med bredd `90%` och centrerad med `margin: 0 auto;`.

![Alt text](../.gitbook/assets/image-15.png)

Boxarna bygger vi sedan med `div.boxar` med `display: grid;` och `grid-template-columns: 1fr 1fr 1fr;` för att skapa tre kolumner med lika stor bredd.

![Alt text](../.gitbook/assets/image-16.png)

Varje box innehåller en bild, en rubrik och en beskrivning. Vi använder `div.box` för att skapa en box med en bild och text.

### Kolumnerna

Vi skapar tre kolumner med `grid-template-columns: 1fr 1fr 1fr;` och `gap: 20px;` för att skapa ett mellanrum mellan boxarna.
