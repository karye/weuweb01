---
description: Med CSS grid är det lätt att skapa ett rutnät.
---

# SVT play

Här är en övning på att återskapa en autentisk webbsida. Vi ska återskapa [SVT Play](https://www.svtplay.se/kategori/for-hela-familjen) med en meny och rutnät med program.

## Resultat

![alt text](../.gitbook/assets/image-138.png)

## Steg 1 - förberedelser - webbrot

* Skapa en mapp som heter **burger-king**
* Skapa en webbsida som heter **meny.html**
* Skapa en CSS-fil som heter **style.css**
* Skapa en mapp **bilder**

### Bilderna

Ladda ned bilderna:

![alt text](../.gitbook/assets/image-139.png)

![alt text](../.gitbook/assets/image-140.png)

![alt text](../.gitbook/assets/image-141.png)

![alt text](../.gitbook/assets/1736410379.avif)

![alt text](../.gitbook/assets/1734101930.avif)

![alt text](../.gitbook/assets/1733753000.avif)

## Steg 2 - skapa HTML-sidan

### HTML-strukturen

Såhär ser sidans HTML-struktur ut:

* `.kontainer` - en stor box för hela sidan
  * `.sidhuvud` - en box för sidhuvudet (CSS `flex`)
    * `.meny` - en meny med logga och menyval
      * `img` - en logga
      * `a` - menyval
    * `.konto` - en box
      * `a` - knappar för sök och inloggning
    * `h1` - en rubrik
    * `p` - en beskrivning
    * `.flikar` - en box för varje program
      * `a` - en beskrivning av programmet
    * `.rutnät` - en box för alla program (CSS `grid`)
      * `.program` - en box för varje program
        * `h2` - en rubrik på programmet
        * `img` - en bild på programmet
        * `p` - en beskrivning av programmet

## Steg 3 - snygga till sidan med CSS

### CSS-reglerna

* Infoga först CSS-reset
* Infoga alla CSS-regler som motsvarar de taggar vi använder
* Gå till [Google Fonts](https://fonts.google.com) och välj liknande typsnitt

```css	
/* Enkel CSS-reset */
html {
    box-sizing: border-box;
}
*, *:before, *:after {
    box-sizing: inherit;
}
body, h1, h2, h3, h4, h5, h6, p, ul {
    margin: 0;
    padding: 0;
}
img {
    max-width: 100%;
}
```

### Färgerna

Det finns två bakgrundsfärger som används på sidan:

* '#1B1614' - en nästan svart färg 
<div style="border:1px solid #AAA;background-color: #1B1614; width: 100px; height: 100px;"></div>

* '#211D1B' - en mörkgrå färg
<div style="border:1px solid #AAA;background-color: #211D1B; width: 100px; height: 100px;"></div> 