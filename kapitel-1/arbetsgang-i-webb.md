---
description: Steg-för-steg metod för att bygga en webbsida
---

# Arbetsgång i webb

När skapar en webbsida kan det vara bra att följa en arbetsgång. Det är lätt att glömma bort något viktigt steg. 

1. Det första steget är att skapa webbroten
  * Skapa en **mapp** för webbsidan. 
  * Därefter skapar vi en **HTML-fil** och en **CSS-fil**. 
  * Eventuellt skapar vi en **mapp** för bilder och andra filer som ska användas på webbsidan.
1. Det andra steget är att skriva grundkoden i HTML-filen. 
  * Vi använder oss av **HTML-element** för att bygga upp sidan. 
1. Det tredje steget är att skriva CSS-regler i CSS-filen. Vi använder oss av **CSS-regler** för att bestämma hur HTML-elementen ska se ut.
  * Skriv upp alla regler som motsvarar de HTML-element vi använder. De är tomma till en början.
  * Välj typsnitt och färger.
  * Välj bakgrundsbilder.
  * Ställ in bredd (`width`) på tex sidan.
  * Ställ in `margin` och `padding` på elementen.
  * Osv..
1. Det sista steget är att testa webbsidan i webbläsaren. Vi kan använda oss av **inspektera** för att se hur webbsidan ser ut. 
  * Validera HTML-koden.
  * Validera CSS-koden.
  * Vi kan även använda oss av **responsiv vy** för att se hur webbsidan ser ut på olika skärmstorlekar.
  * .. och åtgärda felen!

![](<../.gitbook/assets/image (46).png>)

{% file src="/.gitbook/assets/Arbetsgång i webb.pdf" %}
