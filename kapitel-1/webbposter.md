---
description: Träna på bakgrundsbilder och margin/padding
---

# Webbposter 2001

I den här övningen tränar vi på användningen av bakgrundsbilder. Vi använder `background-position` för att styra vilken del av bilden som ska visas.

## Resultatet

![](<../.gitbook/assets/image (23).png>)

## Video 

### 9: CSS Text Styling Tutorial | Basics of CSS 

{% embed url="https://youtu.be/azZCW24XtT4?si=vUy8BXzfkbVGZzSW" %}

## Steg 1 - förberedelser - webbrot

* Skapa en mapp som heter **space-odyssey**
* Skapa en webbsida som heter **space.html**
* Skapa en CSS-fil som heter **style.css**
* Skapa en mapp **bilder**

## Bilden på jorden

* Ladda ned bilden på jorden från Wikipedia:

{% embed url="https://upload.wikimedia.org/wikipedia/commons/thumb/9/97/The_Earth_seen_from_Apollo_17.jpg/767px-The_Earth_seen_from_Apollo_17.jpg" %}

## Steg 2 - skapa HTML-sidan

![](<../.gitbook/assets/image (24).png>)

## Steg 3 - snygga till sidan med CSS

* Vi använder bilden som bakgrundsbild
* Texten som ligger i `<h1>` får också lite CSS för utseendet

```css
body {
    background: #000 url(earth.jpg) no-repeat 750px 0;
    background-size: 1000px 1000px;
}
h1 {
    font: bold 140px sans-serif;
    color: #FFF;
    width: 500px;
}
```

### Hexfärgkoder

Färger på webben anges med hexfärgkoder. Några vanliga färger är:

* `#000` är svart
* `#FFF` är vit

Och allt däremellan är olika nyanser av grått:

* `#111` är mörkgrå
* `#222` är mörkare grå
* ...
* `#EEE` är ljusgrå
* `#FFF` är vit

### Hur man styr textens placering

* CSS margin kan användas till att flytta texten

### CSS shorthand

Egenskapen `background-position` kan skrivas med två värden. Det första värdet anger horisontell placering och det andra värdet anger vertikal placering.

Se [https://developer.mozilla.org/en-US/docs/Web/CSS/Shorthand\_properties](https://developer.mozilla.org/en-US/docs/Web/CSS/Shorthand\_properties)

## Variationer

### Bakgrundsbilden ovan

För att få jorden att hamna på rätt plats använder vi `background-position` med värdena `750px 0`.

```css
body {
    background: #000 url(earth.jpg) no-repeat 50px -750px; 
}
```

![](<../.gitbook/assets/image (25).png>)

### Bakgrundsbilden nedan

En annan variant är att flytta bakgrundsbilden nedåt. Då använder vi `background-position` med värdena `50px 600px`.

```css
body {
    background: #000 url(earth.jpg) no-repeat 50px 600px;
}
```

![](<../.gitbook/assets/image (26).png>)
