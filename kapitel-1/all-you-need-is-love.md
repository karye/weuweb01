---
description: Träna radbrytning och att styla med CSS
---

# "All You Need is Love"

I den här övningen ska vi skapa en webbsida om låten "All You Need is Love" av Beatles. Vi använder grundläggande HTML-taggar för att formatera texten, dvs `h1, p, strong, em, img src`.

I låttexten finns det en radbrytning som vi måste lägga till för att texten ska bli läsbar. Vi använder taggen `<br>` för att göra det.

Vi använder även sk CSS tag-regler för att styla sidan. För varje tagg vi använder skapar vi en CSS-regel. Vi använder även Google Fonts för att välja typsnitt.

## Resultatet

![Resultatet](../.gitbook/assets/image-18.png)

## Video

### Länk och länkars utseende - HTML och CSS nybörjarguide del 7

{% embed url="https://youtu.be/AFDjBU1PoLU?si=oO3LVd0wslNKQtdc" %}

### Bilder och bildlänkar - HTML och CSS nybörjarguide del 9

{% embed url="https://youtu.be/uIqUzJA2siE?si=vT6OsFw7B2QOoObv" %}

### Egna fonter på hemsidan med Google fonts

{% embed url="https://youtu.be/A-lkKqwOrsw?si=zIz6zr_oZ3Fq3QgP" %}

### CSS Text Styling Tutorial | Basics of CSS 

{% embed url="https://youtu.be/azZCW24XtT4?si=vUy8BXzfkbVGZzSW" %}

## Steg 1 - förberedelser - webbrot

* Skapa en mapp som heter **love**
* Skapa en fil som heter **index.html**
* Skapa en ny fil som heter **style.css**
* Skapa en mapp **bilder**

## Hitta låttexten

* Hämta texten från [http://www.lyricsfreak.com/b/beatles/all+you+need+is+love\_10026698.htm](http://www.lyricsfreak.com/b/beatles/all+you+need+is+love\_10026698.html)

![](<../.gitbook/assets/image (9).png>)

## Steg 2 - skapa HTML-sidan

Såhär ser sidans HTML-struktur ut:

* `h1` - en rubrik
* `p` - ett stycke
  * `br` - en radbrytning
  * ... och så vidare
* `p` - ett stycke
  * `br` - en radbrytning
  * ... och så vidare
* `p` - ett stycke
  * `a` - en länk

### Formatera låttexten

* Använd `<h1>` till rubriken
* Använd `<p>` till hela versen
* Men en radbrytning behövs för att läsa låten rätt `<br>`, se bilden:

![](<../.gitbook/assets/image (11).png>)

### Infoga och formatera Wikipedia-text om låten

* Text om låten finns på Wikipedia: [https://sv.wikipedia.org/wiki/All\_You\_Need\_Is\_Love](https://sv.wikipedia.org/wiki/All\_You\_Need\_Is\_Love)

![](<../.gitbook/assets/image (10).png>)

### Infoga källor för att tacka upphovsmännen/kvinnor

* Texterna har vi kopierat, så då måste vi berätta vad de kommer ifrån

![](<../.gitbook/assets/image (12).png>)

## Steg 3 - snygga till sidan med CSS

Nu är det dags att styla sidan med CSS. Vi använder taggarna som vi använder i HTML för att skapa CSS-regler.

### Skapa CSS-regler

* Infoga alla CSS-regler som motsvarar de taggar vi använder

![](<../.gitbook/assets/image (22).png>)

### Infoga bakgrundsbild

* Ladda en valfri bild från [Unsplash](https://unsplash.com/) \
  Välj en bild som är minst 2000px bred
* Lägg in bilden i mappen **bilder**
* Infoga bilden i CSS med `background-image` på `body`-regeln såhär:

```css
body {
  background-image: url("./bilder/unsplash.jpg");
}
```

* Ytterligare CSS-regler för bakgrundsbilden

```css
body {
  background-image: url("./bilder/unsplash.jpg");
  background-size: cover; /* täck hela sidan */
  background-attachment: fixed; /* bilden stannar fast när man skrollar */
}
```

### Styla texten

* Börja med att välja typsnitt på Google Fonts: [https://www.google.com/fonts](https://www.google.com/fonts#).

![alt text](../.gitbook/assets/image-103.png)

* Välj två typsnitt
  * Ett **Display** för rubriken
  * Ett **Serif** för stycken

![alt text](../.gitbook/assets/image-104.png)

* Koden för att bädda in typsnitten

![alt text](../.gitbook/assets/image-105.png)

* Klistra länkkoden i `<head>`

#### Fyll på reglerna

* Klistra in CSS för typsnitt och välj typsnitt med `font-family`
* Välj textstorlek med `font-size`
* Välj textfärg med `color`

![](<../.gitbook/assets/image (21).png>)

## Uppgift

* Skapa en webbsida om din favoritlåt

1. Skapa en ny webbrot som heter **favvo-lat**
1. I den mappen skapar du också en ny mapp som heter **bilder**
1. Ladda ned en bild på din favoritartist och lägg in den i **bilder**
1. I webbroten skapar du en HTML-sida som heter **lat.html** och en CSS-sida som heter **style.css**
1. I **lat.html** skriver du en kort text om din låt
1. I **style.css** skriver du CSS-regler för att styla sidan
1. Du kan använda dig av [https://www.google.com/fonts](https://www.google.com/fonts#) för att välja typsnitt