---
description: Hitta alla HTML-fel i koden
---

# Uppgift: Finn alla fel

## Introduktion
I den här uppgiften kommer vi att praktisera vår förmåga att identifiera och korrigera fel i HTML-kod. Att kunna upptäcka och åtgärda fel är en viktig färdighet för alla som arbetar med webbutveckling.

## Syfte
Syftet är att förbättra din kunskap om HTML-syntax och förstå hur felaktig kod kan påverka en webbsidas funktion och utseende.

## Uppgiftsbeskrivning
Nedan finns en bit HTML-kod med flera inbyggda fel. Din uppgift är att hitta och lista alla fel. För varje fel du hittar, förklara varför det är fel och hur det kan korrigeras.

```html
<!DOCTYPE HTML>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Tralala lilla molntuss</title>
</head>
<body>
	<h1>Tralala lilla molntuss<h1/>
    
	<img src="./c-dustin-K-Iog-Bqf8E-unsplash.jpg">
    
	<p>Ha-ha nu har jag gjort det igen<br>
	jag har varit dum mot mig själv<br>
	jag är så bra på sånt, eller hur?</p><br>
    
	<p>tralala lilla <em>molntuss</em>... tralala lilla <em>molntuss</em>
        kom hit skall du få en <strong>puss</strong></p></p> 

	<p>Dom kan klaga om dom vill<br>
	jag har slutat bry mig om sånt<br>
	jag är glad att jag finns till<br>
	det breddar upp min horisont<br><br>

    <h1>Bilder</h1>
	<p><a href="http://www.bobhund.se">Bob Hund</p></a>

    <h2>Erkännande</h3>
    <a>Mer info om Bob Hund finns på Wikipedia</a>
</body>
<a>Bild av C Dustin på Unsplash</a>
</html>
```

Genom att korrigera dessa fel kommer HTML-koden att följa rätt syntax och därmed visas korrekt i webbläsare. Det är viktigt att alltid kontrollera och testa din kod för att säkerställa att den är fri från fel och fungerar som avsett.