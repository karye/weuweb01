---
description: Träna på HTML tabell, och styla med CSS
---

# Veckomeny

Nu ska vi skapa en veckomeny med hjälp av en HTML-tabell och CSS. En tabell skapas med HTML-taggen som heter `<table>` och den består av rader `<tr>` och celler `<td>`.

## Resultatet

![](<../.gitbook/assets/image (36).png>)

## Video

### HTML & CSS - Tabeller - Webbutveckling

{% embed url="https://youtu.be/pEtbDj5pMZk?si=rtFzH4t8NWtJHrP8" %}

### Egna fonter på hemsidan med Google fonts

{% embed url="https://youtu.be/A-lkKqwOrsw?si=zIz6zr_oZ3Fq3QgP" %}

## Grundkoden för tabell

Enklaste formen av tabell kan se ut såhär:

```html
<table>
  <tr>
    <td>Johnny</td>
    <td>Doe</td>
  </tr>
  <tr>
    <td>Janne</td>
    <td>Doe</td>
  </tr>
</table>
```
## Steg 1 - förberedelser - webbrot

* Skapa en mapp som heter **veckomeny**
* Skapa en webbsida som heter **meny.html**
* Skapa en CSS-fil som heter **style.css**
* Skapa en mapp **bilder**

## Steg 2 - skapa HTML-sidan

### HTML-strukturen

Såhär ser sidans HTML-struktur ut:

* `table` - en tabell
  * `tr` - en rad
    * `td` - en cell
    * `td` - en cell
    * ...
  * `tr` - en rad
    * `td` - en cell
    * ...

### Skapa en tabell med 5 kolumner

Välj rätter från [https://www.ica.se/recept/buffe](https://www.ica.se/recept/buffe/)

![](../.gitbook/assets/image-69.png)

### Skapa en första rad för veckodagarna

* Infoga en tabellrubrik med `<caption>`
* Skapa en rad celler med `<tr>`
* Fyll på med 5 celler med `<td>`

![](../.gitbook/assets/image-71.png)

### Skapa en till rad för rätterna

* Duplicera raden

![](../.gitbook/assets/image-72.png)

### Skapa en 3:e rad för bilderna

* Skapa en ny rad celler
* För att komma åt bilden klicka på receptet
* Högerklicka på bilden och välj **Spara som... **ned i mappen **bilder**
* Infoga sen bilderna med `<img>` i varje cell
* Omvandla första radens celler till kolumnrubriker `<th>`

![](<../.gitbook/assets/image (33).png>)

## Steg 3 - snygga till sidan med CSS

### Infoga alla tomma CSS-regler

![](../.gitbook/assets/image-73.png)

### Styla tabellen och texten

* Ställ in tabellens bredd med `width`
* Ställ in även bildernas bredd med `width`
* Välj en bakgrundsfärger med `background-color`

![](../.gitbook/assets/image-74.png)

### Styla cellerna

* Gör cellerna lite luftigare med `padding`
* Lägg till kanter med tex `border: 1px solid #ccc;`

![](../.gitbook/assets/image-78.png)

## Uppgift

* Välj 2 typsnitt från [Google Font](https://fonts.google.com)
  * Använd **HTML-link** för att infoga Google Font
  * Styla rubrik och text med typsnitten
* Styla sidans bakgrund med bild
  * Leta på [Unsplash](https://unsplash.com) efter en bakgrundsbild
  * Vi du använda ett mönster, filtrera på **Patterns**
  * Använd CSS `background-image: url();` för att infoga bilden
* Prova även att styla bakgrund med CSS linear gradient
  * Se [CSS Gradient](https://cssgradient.io)
* Prova att ge varje cell en skugga med CSS `box-shadow`
  * Se [CSS Box Shadow](https://cssgradient.io)