---
description: Här är en lista på tutorials för att träna på egen hand
---

# På egen hand

## Grunder i webbutveckling

### Skapa "Evas frukt" hemsidan

![Alt text](../.gitbook/assets/image-6.png)

* Skapa en tom webbrot i VS Code
* Följ instruktionerna på [https://webbkoda.se/evas-frukt](https://webbkoda.se/evas-frukt/)

{% file src="../.gitbook/assets/evas-frukt-hemsida.zip" %}

### Skapa "Hit-och-dit" webbplatsen

![Alt text](../.gitbook/assets/image-7.png)

* Skapa en tom webbrot i VS Code
* Följ instruktionerna på [https://webbkoda.se/hit-och-dit-resor](https://webbkoda.se/hit-och-dit-resor/)

{% file src="/.gitbook/assets/hit-och-dit.zip" %}

### Träna med webbskolans videor

{% embed url="https://www.youtube.com/watch?v=8Xp413bo2j4&list=PLuc3JXzcNTZSYxj8Q32SIbR7wt7fgi7of" %}

* Skapa en tom webbrot i VS Code
* Följ instruktionerna och koda i VS Code

