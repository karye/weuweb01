---
description: Uppgift om att skapa en portfolio
---

# Skapa en portfolio

## Uppgift
Du ska skapa en enkel portfoliowebbsida där du visar upp tre **projekt**, **intressen** eller **hobbies**. 

Sidan ska ha en startsida med en kort presentation och tre sektioner som presenterar varje projekt/intresse. 

Du kan inspireras av övningarna "Bloggen" och "Bildgalleri" för att skapa en enkel webbplats med HTML och CSS.

### Krav

#### Webbplatsen ska innehålla följande
1. Skriv en kort **presentation** om dig själv.
1. Skapa **tre sektioner** som representerar dina projekt/intressen.
1. Använd **bilder**, text och länkar för att beskriva varje projekt/intresse.
1. Använd **typsnitt** från Google Fonts och färger från en **färgpalett**.

#### HTML-struktur
1. Skapa en fil som heter **index.html**.
1. Använd en `<div>` med en klass för att skapa en huvudkontainer som samlar alla element på sidan.
1. Använd taggarna `<h1>`, `<h2>` ...,  `<div>`, `<img>`, `<p>`, `<a>` för att skapa en struktur.	
1. En tabell `table` eller lista med `ul` och `li` för att lista dina projekt/intressen.
1. Kolla på [w3schools](https://www.w3schools.com/html/) om du är osäker på HTML.

#### CSS-styling
1. Skapa en CSS-fil som heter **style.css** och länka den till din HTML-fil.
1. Inkludera en **CSS-reset** 
1. Lägg till styling för:
   * Sidans bakgrund och textfärger.
   * Rubriker och brödtext.
   * Länkar och hover-effekter.
   * Bildstorlekar och marginaler.
   * Kolla på [w3schools](https://www.w3schools.com/css/) om du är osäker på CSS.

#### Flexbox-layout:
   1. Använd **CSS flex** för att arrangera innehållet inom varje sektion

![alt text](../.gitbook/assets/image-117.png)

#### Material
1. Välj bilder på tex [Unsplash](https://unsplash.com/) eller [Pexels](https://www.pexels.com/) som representerar dina projekt/intressen.
1. Välj en palett från [Coolors](https://coolors.co/) eller [Color Hunt](https://colorhunt.co/) för att styla din webbplats.

### Utmaning
1. Lägg till en enkel navigeringsmeny längst upp som länkar till de olika sektionerna, se [anchor Links](https://marksheet.io/html-links.html#anchor-targets).
1. Skapa en responsiv design med hjälp av media queries så att layouten anpassas för mindre skärmar.

## Inlämning
* Gör commits och pusha till GitHub efter varje större ändring.