---
description: Text på sidan om bilden med CSS flex
---

# Bildgalleri

I den här övningen ska vi skapa ett bildgalleri. Vi använder oss av en `div`-box för att skapa en **kontainer**. En `div`-box blir som en behållare som vi kan stoppa andra element i.

I varje ruta ligger bilderna ömsom till vänster och ömsom till höger om texten. Vi använder oss av CSS-egenskapen `flex` på ruta-elementet för att bestämma hur bilderna och texten skall ligga. Vi kan sedan använda CSS `flex-direction:row-reverse` för att bestämma att bilden skall ligga till höger om texten och `flex-direction:row` för att bestämma att bilden skall ligga till vänster om texten.

## Resultat

![Alt text](../.gitbook/assets/image-12.png)

## Video

### CSS Classer och styletaggen - HTML och CSS nybörjarguide del 5

{% embed url="https://youtu.be/VHa9bFFPtc8?si=Mh_UB4Zhk6R6Sk6r" %}

### Så funkar CSS Flexbox

{% embed url="https://youtu.be/waVu2pJ2PUU?si=2vsw8YVMvadc9pr4" %}

## Steg 1 - förberedelser - webbrot

* Skapa en mapp som heter **bildgalleri**
* Skapa en webbsida som heter **galleri.html**
* Skapa en CSS-fil som heter **style.css**
* Skapa en mapp **bilder**

## Steg 2 - skapa HTML-sidan

### HTML-strukturen

Såhär ser sidans HTML-struktur ut:

* `.kontainer` - en stor box för hela sidan
  * `h1` - en rubrik
  * `h2` - en mellanrubrik
  * `.ruta` - en box för varje bild
    * `img` - en bild
    * `h3` - en bildtext
    * `p` - en beskrivning

### Grundkoden

* Börja med grundkoden
* Välj 5 foton från [Nasa Moon galleries](https://moon.nasa.gov/galleries/images/?page=1&per_page=25&order=created_at+desc&search=&href_query_params=category%3Dimages&condition_1=1%3Ais_in_resource_list&category=51%2C-207)
* Kopiera rubriken och bildtexten och infoga i HTML-koden
* Fyll i alla HTML-elementen som bygger upp sidan

![Alt text](../.gitbook/assets/image-9.png)

## Steg 3 - snygga till sidan med CSS

### CSS-reglerna

* Infoga först CSS-reset

```css	
/* Enkel CSS-reset */
html {
    box-sizing: border-box;
}
*, *:before, *:after {
    box-sizing: inherit;
}
body, h1, h2, h3, h4, h5, h6, p, ul {
    margin: 0;
    padding: 0;
}
img {
    max-width: 100%;
    height: auto;
}
```
* Infoga alla CSS-regler som motsvarar de taggar vi använder i HTML-koden
* Gå till [Google Fonts](https://fonts.google.com) och välj två typsnitt
* Välj en bakgrundsbild till body-elementet

### Layouten

* Ställ in bredden på `.kontainer` till 800px
* Lägg på en padding på 50px
* Centrera `.kontainer` med `margin: auto`

![Alt text](../.gitbook/assets/image-10.png)

### Boxarna med bild och text

* Välj en bakgrundsfärg på `.ruta`
* Lägg på en padding på 50px
* Lägg till en avstånd mellan boxarna med `margin-bottom: 20px`
* Använd `display: flex` för att lägga bild och text bredvid varandra
* Använd också `flex-direction: row-reverse` för att lägga bilden till höger om texten

![Alt text](../.gitbook/assets/image-11.png)