# E-sport webbsida

![alt text](<../.gitbook/assets/Skärmbild 2024-10-25 143151.png>)

I denna övning ska du skapa en webbsida för e-sportentusiaster som visar upp nyheter om lag och kommande matcher inom e-sportvärlden. Du kommer att använda HTML för att strukturera innehållet och CSS för att designa sidan.

## Resultat

Efter att ha slutfört övningen ska du ha en webbsida som visar en modern layout med en bakgrundsbild, sektioner för laginformation och kommande matcher samt en sidfot med källhänvisning.

## Steg 1 - Skapa mapp och filer

1. Skapa en ny mapp för projektet och döp den till **esport**.
2. Skapa två filer i mappen:
   - **index.html**: HTML-strukturen för webbsidan.
   - **style.css**: CSS-stilmallar för sidans design.

## Steg 2 - Strukturera HTML-filen (index.html)

1. Öppna **index.html** och gör följande:
   - Lägg till en huvudrubrik (h1) med texten "eSport Arena".
   - Under huvudrubriken, lägg till ett stycke (p) med klassen `intro` som kort introducerar sidan.
   - Skapa en sektion för lagpresentation med två kolumner, varje kolumn ska ha en bild och en kort beskrivning.
   - Lägg till en sektion för kommande matcher under lagpresentationen, där varje match presenteras med en bild och en kort beskrivning.
   - Längst ner på sidan, inkludera en sidfot med text som anger källor för bilder och texter.

2. HTML-strukturen ska innehålla:
   - En `<div class="kontainer">` som omsluter allt innehåll.
   - En `<h1>`-rubrik för sidans namn.
   - En introduktionstext med klassen `intro`.
   - En `<div class="ruta">` för varje lag eller match, som innehåller:
      - En bild `<img>` med alt-attribut som beskriver bilden.
      - Ett stycke `<p class="text">` med en kort beskrivning av laget eller matchen.
   - En `<p class="kalla">` som anger källorna för sidans bilder och texter.

## Steg 3 - Lägg till CSS-styling (style.css)

1. Öppna **style.css** och följ dessa riktlinjer för styling:
   - **Bakgrund**: Lägg till en bakgrundsbild som täcker hela sidan, är fixerad och anpassas till skärmens storlek.
   - **Typsnitt**: Använd typsnitt från Google Fonts, t.ex. "Faster One" för rubriken och "Roboto Slab" för övrig text.
   - **CSS-reset**: Gör en enkel reset genom att sätta marginal och padding till 0 för grundelementen.
   - **Kontainer**: Stil för `.kontainer` med en vit, halvt genomskinlig bakgrund, padding, och rundade hörn.

### Styla layouten

1. Välj typsnitt och färger för huvudrubrik och introduktionstext på Google Fonts.
1. Ange `width` och `margin` för att centrera innehållet i `.kontainer`. Ange också en `padding` för att skapa mellanrum runt innehållet.
1. Välj bakgrundsfärger för `body`, `.kontainer`, `.ruta`, och `.kalla` för att skapa en enhetlig design.

### Bilderna

1. Använd Picsum-bilder för lag och matchbilder. Ange `width` på ca 30% för att skala bilderna korrekt.

### Lägg texten till höger om bilderna

1. Vi vrider på flödet för `.ruta` genom att använda `display: flex` för att placera texten till höger om bilden. 
1. Styla texten med `padding`, `margin`, och `font-size` för att skapa en snygg layout.
1. Styla bilderna med `border` för en snygg ram runt dem, och `border-radius` för att göra hörnen rundade.

### Skapa två kolumner

1. På samma sätt som ovan, använd `display: flex` för att skapa två kolumner för lagpresentationen och kommande matcher.
1. Lite avstån mellan kolumnerna kan skapas `gap`-egenskapen.

### Sidfoten

1. Styla sidfoten med en annan bakgrundsfärg och textfärg. Använd `text-align` för att centrera texten.

### Vertikala marginaler

1. Använd `margin-bottom` för att skapa vertikala marginaler mellan elementen för att göra sidan mer lättläst.

![alt text](../.gitbook/assets/image-115.png)

![alt text](../.gitbook/assets/image-116.png)

## Steg 4 - Kontrollera slutresultatet

1. Öppna webbsidan i en webbläsare och säkerställ att:
   - Sidan visar rubrik och introduktionstext på ett snyggt sätt.
   - Lagpresentation och kommande matcher visas i två kolumner.
   - Bilder och texter i rutorna är placerade enligt instruktionerna.
   - Sidfoten är korrekt stylad och centrerad längst ner på sidan.
