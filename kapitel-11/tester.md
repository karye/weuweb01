# Testa webbplatsen

## W3C Validator

{% embed url="https://youtu.be/EppZLro-Pfg?si=Hs49YqtiNMxwg9II" %}

För att testa en webbplats kan du använda W3C Validator. Den kontrollerar att HTML och CSS är korrekt skrivet.

Här är två verktyg för att validera HTML:

* [W3C Validator](https://validator.w3.org/)

![alt text](../.gitbook/assets/image-84.png)

* [HTMLhint](https://marketplace.visualstudio.com/items?itemName=HTMLHint.vscode-htmlhint) validerar HTML i VS Code

## Lighthouse

I **Chrome** finns ett verktyg som heter **Lighthouse**. Det testar webbplatsen och ger förslag på förbättringar.

1. Öppna **Chrome** och gå till webbplatsen
2. Högerklicka och välj **Inspektera**

![alt text](../.gitbook/assets/image-81.png)

3. Klicka på **Lighthouse** och välj **Generate report**

![alt text](../.gitbook/assets/image-82.png)

4. Lighthouse testar webbplatsen och ger förslag på förbättringar

![alt text](../.gitbook/assets/image-83.png)
