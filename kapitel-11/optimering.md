# Optimeringar av media

## Spara bilder i rätt format

Det bästa bildformatet för webben är **.webp**. Det är ett format som är utvecklat av Google och som ger bra kvalitet och liten filstorlek. Det är dock inte kompatibelt med alla webbläsare. Därför kan du använda **.jpg** som alternativ. Det är ett format som är kompatibelt med alla webbläsare och som ger bra kvalitet.

## Skala ned bilder

Om du använder en bild som är större än den storlek som visas på webbplatsen, kan du skala ned den. Det gör att filstorleken blir mindre och att webbplatsen laddas snabbare. Tex bilden nedan är 2000x2000 pixlar och 2,5 MB stor. Om du skalar ned den till 500x500 pixlar blir den 100 KB stor.