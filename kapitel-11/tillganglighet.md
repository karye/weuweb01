# Tillgänglihet

När man skapar en webbplats är det viktigt att tänka på tillgänglighet. Det innebär att webbplatsen ska vara användbar för alla, oavsett funktionsnedsättning. Här är några tips på hur du kan göra din webbplats mer tillgänglig.

## Färgblindhet

Färgblindhet är vanligt och det är viktigt att tänka på när du designar en webbplats. Använd inte bara färg för att förmedla information. Använd även text eller symboler.

### Protaopia & Deuteranopia

Protaopia och Deuteranopia är två vanliga former av färgblindhet. Använd inte röd och grön bredvid varandra.

Gå in i Chrome Dev Tools och välj **More tools** -> **Rendering** -> **Emulate vision deficiencies** för att testa färgblindhet.

![alt text](../.gitbook/assets/image-149.png)

## Skärmläsare

Skärmläsare är ett hjälpmedel för synskadade. Använd **alt**-taggar för bilder och **aria-label** för länkar.

```markup
<img src="bild.jpg" alt="Beskrivning av bilden">
<a href="sida.html" aria-label="Beskrivning av länken">Länk</a>
```

## Tangentbord

Många användare använder tangentbordet för att navigera på webbplatsen. Se till att det går att navigera med tangentbordet.

## Responsiv design

Använd **media queries** för att skapa en responsiv design. Testa webbplatsen i olika webbläsare och på olika enheter.

