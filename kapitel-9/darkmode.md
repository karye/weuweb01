---
description: Dark mode med CSS och JavaScript.
---

# Byta till Dark Mode med CSS och JavaScript

## Introduktion

Dark mode är ett populärt användargränssnitt som ger användare möjlighet att byta färgschemat på en webbsida från ljus till mörk. Detta kan vara särskilt användbart för personer som arbetar i mörka miljöer eller som helt enkelt föredrar en mörkare design.

## CSS-regler för Dark Mode

För att skapa en dark mode-funktion på en webbsida, kan du använda CSS-regler för att ändra färgerna på elementen. Här är några exempel på CSS-regler som kan användas för att skapa en dark mode-design:

```css
body.dark {
    background-color: #333;
    color: #fff;
}
```

I detta exempel ändrar vi bakgrundsfärgen till mörkgrå och textfärgen till vit när klassen `dark` läggs till på `<body>`-elementet.

## Knapp för att byta till Dark Mode

För att låta användare byta mellan ljus och mörk läge på en webbsida, kan du skapa en knapp som aktiverar dark mode-funktionen:

```html
<button id="mode">Dark Mode</button>
```
## JavaScript för att byta till Dark Mode

Till slut skapar vi Javascript-koden som körs när man klickar på knappen. \
Skapa först en ny Javascript-fil **script.js**:

```js
// Lyssna på klick på knappen
/* Light/dark mode knapp */
document.querySelector('#mode').addEventListener('click', function() {
    /* Vit byter css-regel med `toggle` */
    document.body.classList.toggle('dark');
    document.querySelector('h1').classList.toggle('dark');
});
```
