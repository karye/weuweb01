# Checklista

Denna checklista är en sammanfattning av de olika moment som ska ingå i projektet. Den kan användas för att säkerställa att alla delar är täckta och för att hålla koll på framstegen. Varje punkt kan markeras som klar när den är färdigställd, vilket hjälper till att strukturera arbetet och säkerställa att alla aspekter är täckta.

## Projektplan

- [ ] **Eleven upprättar en projektplan**
  - [ ] Beskriver sidans/webbplatsens syfte (E)
  - [ ] Skapar en enkel skiss/wireframe som visar sidstrukturen (E)
  - [ ] Skapar en detaljerad wireframe för dator som visar sidstrukturen samt innehåll med information om fonter, färger, bilder & layout. (C)
  - [ ] Beskriver den tilltänkta målgruppen (ålder, intressen, kön etc) (C)
  - [ ] Detaljerade wireframe som visar sidstrukturen samt innehåll för mobil (A)
  - [ ] Detaljerade wireframe som visar sidstrukturen samt innehåll för tablett (A)
  - [ ] Beskriver designdetaljer som font och färgkoder samt layout för olika enheter (A)
  - [ ] Infogar en sitemap (sidkarta) för webbplatsen, alla sidor i sidkartan behöver inte finnas (A)

## Genomförandet

### Självstyrning

- [ ] **Eleven utvecklar utifrån planen en produkt..**
  - [ ] Lämnar INTE in i tid (efter deadline) med liten commit-historik (5 > commits) (E)
  - [ ] Lämnar in i tid med hjälp av lärare med en kontinuerlig commit-historik (> 10 commits) (C)
  - [ ] Lämnar in i tid utan hjälp från lärare med god välformulerad commit-historik (> 20 commits) (A)

### Kodstandard

- [ ] **I arbetet utvecklar eleven kod som följer standarder och omfattar teknikerna för märkspråk och stilmallar.**
  - [ ] Följer HTML5 standard (inga fel i VS Code) (E)
  - [ ] Rätt användning av element: rubriker, texter, listor, bilder & divar (E)
  - [ ] En enkel layout med en layouthanterare (flex eller grid) (E)
  - [ ] Användning av enkla CSS-regler (ex färg) för element selektorer tex h1 (E)
  - [ ] Rätt ordning på rubriker h1 -> h6 (fallande) (C)
  - [ ] Användning av klass/id-selektorer & namnger dessa på genomtänkt sätt (C)
  - [ ] Använder flex och grid layout (C)
  - [ ] Sätter ut antalet kolumner med grid (C)
  - [ ] Sätter ut element på specifika platser i grid (grid-template-areas) (A)
  - [ ] Sidan inklusive text bilder och layout ser bra ut på samtliga enheter (A)
  - [ ] Användning av ARV-CSS-selektorer ex .klass > barn (A)
  - [ ] Layouten följer 1-2-3 (kolumner) för dator > läsplatta > mobil (responsiv) (A)
  - [ ] Sidan använder input-element med labels (A)
  - [ ] Använder rätta semantiska element (A)
  - [ ] Sidan använder dynamiska måttenheter som rem, em, vh/vw eller liknande (istället för px) (A)

## Javascript

- [ ] **I produkten infogar eleven enkla skript**
  - [ ] Inget javascript (E)
  - [ ] Skriptet ändrar på element på sidan (ändrar på DOM-trädet) (C)
  - [ ] Lägger till/ändrar/ta bort element på sida (A)

## Dokumentation

### Text/bilder/media

- [ ] **I arbetet bearbetar eleven text bilder och eventuellt annan media**
  - [ ] Sidans text är på svenska (E)
  - [ ] Använder 1 bild på webbsidan (E)
  - [ ] Sidan innehåller text & rubrik (h1) (E)
  - [ ] Rätt användning av HTML-elementen för textlänkar & bilder (E)
  - [ ] Använder bilder och sparat dessa i webp-formatet (C)
  - [ ] Anpassar mot tillgänglighet med lang-attribut, alt-attribut med relevant text (C)
  - [ ] Använder minst 3 olika färger (exempel 1 för text, 1 för bakgrund, 1 för bakgrund i kontainer-element) (C)
  - [ ] Använder font-family (C)
  - [ ] Bilder anpassar sig till efter enheten dator > läsplatta > mobil (responsiv) (A)
  - [ ] Använt font-family med google-fonts el liknande och web-safe fonts som backup (A)
  - [ ] Text-storleken är olika dator > läsplatta > mobil (responsiv) (A)

## Automatiserade tester med Lighthouse

- [ ] **Produkten är av tillfredsställande kvalitet och följer etablerad praxis vilket eleven kontrollerar med tester.**
  - [ ] Inga fel i HTML/CSS i VS Code (E)
  - [ ] Genomför LightHouse Test och dokumenterar resultat (C)
  - [ ] Genomför LightHouse Test, dokumenterar & ger förklaring samt lösning, ger förslag på hur resultaten ska förbättras (A)

## Tester i webbläsare

- [ ] **Eleven testar produkten i webbläsare.**
  - [ ] Testar i 1 webbläsare Google Chrome (E)
  - [ ] Testar i 2 webbläsare Chrome & Mozilla Firefox (C)
  - [ ] Testar i 3 webbläsare Chrome, Firefox & Edge + rapport (A)

## Anpassning (testa responsivitet)

- [ ] **Eleven testar också produkten på någon plattform och vidtar åtgärder för att åstadkomma (responsiv)**
  - [ ] Testar webplatsen på 1 laptop/stationär (E)
  - [ ] Testar webplatsen på dator & mobil och utvärderar ytligt om UX & funktionalitet (C)
  - [ ] Anpassar sidan/sidornas layout för dator > läsplatta > mobil (responsiv) (C)
  - [ ] Text & länkar är anpassad dator, mobil och tablett skärmstorlek (C)
  - [ ] Bildernas storlek anpassar sig dator, mobil och tablett skärmstorlek (C)
  - [ ] Något element göms i mobil-läge med CSS-regler (A)
  - [ ] Testar webplatsen på dator & mobil, och utvärderar ingående om UX & funktionalitet (A)

## Tillgänglighet (Tester)

- [ ] **Dessutom bygger Eleven en webbplats som med tillfredsställande resultat följer grundläggande principer för tillgänglighet.**
  - [ ] Följer de 4 principerna om tillgänglighet (tydlig, hanterbar, begriplig, robust) (E)
  - [ ] Språket anget i HTML-huvudet till korrekt språk (C)
  - [ ] Bild-elementen har alt-texter med relevant beskrivande text (C)
  - [ ] Använder Mozilla Accessibility test & simulerar synproblem, dokumenterar resultat (C)
  - [ ] Använder CSS-händelser förstärka länkars interaktion med användaren (A)
  - [ ] Använder Silktide Testkit och genomför en fullskalig test och dokumenterar resultat (A)

## Utvärdering

- [ ] **När arbetet är utfört gör eleven en dokumentation av de moment som har utförts och utvärderar med enkla omdömen sitt arbete och resultat.**
  - [ ] Gör en enkel utvärdering där produkten jämförs med ursprungliga planeringen (E)
  - [ ] Gör en detaljerad utvärdering av: layout, responsivitet & tillgänglighet (C)
  - [ ] Förklarar hur sitt javascript fungerar (C)
  - [ ] Eleven ger genomtänkta förslag på förbättringar om sidan utifrån: HTML/CSS-kod funktionalitet (ny funktionalitet), layout i liggande läge (roterad mobilskärm), text, bilder & länkar i liggande läge (roterad mobilskärm) (A)

## Terminologi

- [ ] **Eleven använder terminologi inom området.**
  - [ ] Använder i sin korta utvärdering med viss säkerhet korrekt terminologi (E)
  - [ ] Använder i sin långa utvärdering med viss säkerhet korrekt terminologi (C)
  - [ ] Använder i sin långa utvärdering med säkerhet korrekt terminologi (A)
```
