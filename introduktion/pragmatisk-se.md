# Pragmatisk.se

Denna sida är en uppdaterad sammanställning av information på [pragmatisk.se](https://pragmatisk.se/).

## En webbplats två beståndsdelar

Det finns två övergripande beståndsdelar i en webbplats: innehåll och utseende. Skillnaden mellan de båda är viktiga att förstå.

### Innehåll

Med innehåll menas själva datan, det är rubriker, brödtext, listor, bilder etc. Innehållet är alltid det primära i en webbplats. Anpassa inte innehållet efter utseendet, utan tvärtom.

Som exempel är text där viktiga delar är fetmarkerade inte utseende, utan har en funktion att betona något viktigt i innehållet.

Språket för innehållet är HTML.

### Utseende

Utseende är som en slags mask du lägger på innehållet för att strukturera det visuellt. Du styr val av teckensnitt för rubriker, färg på länkar, bakgrundsbild och struktur.

Som exempel är kursiv text, som inte har som funktion att betona något i innehållet, bara något som rör utseende/designen.

Språket för utseendet är CSS.

## Språk för innehåll

### Introduktion till HTML

För innehåll används ett språk kallat HTML, som markerar olika delar av innehåll för att förklara vad det är, som med en märkpenna. Syftet med att markera innehållet är för att webbläsare, sökmotorer och skärmläsare ska kunna särskilja exempelvis en rubrik från brödtext.

Innehållet markeras med en tagg som skrivs ut före och efter innehållet. För exempelvis en huvudrubrik används taggen `<h1>`. 'h' står för heading och '1' för att det är en huvudrubrik. För underrubriker används `<h2>`, `<h3>` osv. "Pilarna" (vinkelparenteser) före och efter `<taggen>` markerar att det är just en tagg.

De taggar som markerar något innehåll måste "stängas". Det innebär att man anger när taggen ska avslutas. För att markera ut en huvudrubrik ser en komplett tagg för en huvudrubrik ut såhär: `<h1>.SE är en oberoende allmännyttig organisation</h1>`. Notera att den sista taggen har ett snedstreck /, det anger att det är taggens avslut.

Det finns taggar för olika typer av innehåll. Vi kommer visa de mest viktiga och frekvent använda. Standarder styr vilka taggar som finns och hur de ska användas.

### Grunden i ett HTML-dokument

Ett HTML-dokument är som vilket annat textdokument som helst, men består, förutom av innehållet, även av HTML-taggar. Dokumentets filändelse är **.htm** eller **.html** och kan öppnas med valfritt textredigeringsprogram.

För att webbläsaren ska veta att det är ett HTML-dokument den läser, krävs ytterligare HTML-taggar som ger struktur åt dokumentet. Här är ett exempel:

Mitt första HTML-dokument:

```html
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <title>Mitt första HTML-dokument</title>
</head>
<body>
    <h1>.SE är en oberoende allmännyttig organisation</h1>
</body>
</html>
```

!!! Tip

    Notera hur och var varje tagg öppnas samt stängs. HTML-koden är intenderad (indragen) då en tagg innehåller fler taggar.

Förklaring av HTML-taggarna:

- `<!DOCTYPE html>` säger till webbläsaren att dokumenttypen är HTML.
- `<html lang="sv">` öppnar upp dokumentet och anger med attributet 'lang' att det är på svenska (sv).
- `<head>` öppnar upp dokumentets egenskaper.
- `<meta charset="utf-8">` anger att teckenuppsättningen UTF-8 ska användas, vilket inkluderar Å, Ä, och Ö. (Denna tagg stängs inte eftersom den inte markerar något innehåll.)
- `<title>` anger dokumentets titel, som syns i webbläsarens sidhuvud eller i sökresultat från sökmotorer.
- `<body>` öppnar dokumentets "kropp" där allt innehåll finns, såsom huvudrubriken.

#### Ditt första HTML-dokument

Följ stegen nedan för att skapa ditt första HTML-dokument:

1. Markera och kopiera HTML-koden ovan.
2. Klistra in den i en textredigerare.
3. Spara dokumentet som en HTML-fil med filändelsen **.html**.
4. Öppna dokumentet i din webbläsare.

#### Ytterligare 3 taggar

Med `<h1>` färskt i minnet kommer här tre användbara taggar till. Vi fyller på med både tagg och innehåll i det dokument du redan har för enkelhetens skull.

| Tagg        | Förkortning av | Svensk översättning |
|-------------|----------------|---------------------|
| `<h2>`      | Heading 2      | Rubrik 2            |
| `<p>`       | Paragraph      | Stycke              |
| `<a href="">` | Anchor        | Länk                |

Exempel på HTML-dokument med ytterligare taggar:

```html
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <title>Mitt första HTML-dokument</title>
</head>
<body>
    <h1>.SE är en oberoende allmännyttig organisation</h1>
    <p>.SE (Stiftelsen för Internetinfrastruktur) är en oberoende allmännyttig organisation som verkar för positiv utveckling av Internet i Sverige.</p>
    
    <h2>Internetutveckling</h2>
    <p>I enlighet med stiftelsens <a href="https://www.iis.se/om-se/urkund-och-stadgar">urkund och stadgar</a> gynnar vi på olika sätt Internetutvecklingen i Sverige. Det är inom denna verksamhet .SE investerar det överskott som genereras av registreringsavgifterna för domännamn.</p>
</body>
</html>
```

#### Vad då href=""?

Taggar kan ha attribut, vilket är egenskaper man kan ge taggen. Vissa taggar, som exempelvis `<a>`, kräver att man skriver med ett attribut, href.

href är en förkortning av 'hypertext reference', tänk på det som adressen som länken ska peka till. Efter href följer ett likamedtecken (=) och två citationstecken ("") där värdet för attributet skrivs in: `<a href="https://www.iis.se/om-se/urkund-och-stadgar">urkund och stadgar</a>`.

Det är viktigt att alla tecken och mellanslag finns på rätt ställe, annars läses dokumentet fel.

| Tagg        | Attribut-krav | Exempelvärde                |
|-------------|---------------|-----------------------------|
| `<h1>`      |               |                             |
| `<h2>`      |               |                             |
| `<p>`       |               |                             |
| `<a href="">` | href         | https://www.iis.se/         |

!!! Tip

    Kom ihåg att alltid skriva HTML-kod med små bokstäver. Använd också indentering (indrag) för att få en bra överblick och kod som andra lätt kan läsa.

#### Att göra en punkt- eller nummerlista i HTML

I HTML kan du göra två typer av listor: ordnad (1, 2, 3) eller oordnad (punktlista).

##### Oordnad lista (punktlista)

```html
<ul>
    <li>Första punkten</li>
    <li>Andra punkten</li>
    <li>Tredje punkten</li>
</ul>
```

##### Ordnad lista (numrerad)

```html
<ol>
    <li>Första punkten</li>
    <li>Andra punkten</li>
    <li>Tredje punkten</li>
</ol>
```

- `<ol>` står för 'ordered list'.
- `<ul>` står för 'unordered list'.
- `<li>` används för varje punkt på någon av dessa två typer av listor.

#### Du känner nu till 7 taggar

Här kommer en snabb genomgång.

| Tagg        | Förkortning av | Svensk översättning | Attribut-exempel                         |
|-------------|----------------|---------------------|------------------------------------------|
| `<h1>`      | Heading 1      | Rubrik 1            |                                          |
| `<h2>`      | Heading 2      | Rubrik 2            |                                          |
| `<p>`       | Paragraph      | Stycke              |                                          |
| `<a href="">` | Anchor        | Länk                | href="https://www.iis.se/om-se/urkund-och-stadgar" |
| `<ol>`      | Ordered list   | Ordnad lista        |                                          |
| `<ul>`      | Unordered list | Oordnad lista       |                                          |
| `<li>`      | List item      | Listpunkt           |                                          |

#### Avstämning

Med den kod du hade sedan tidigare, och en oordnad lista, bör ditt dokument se ut så här:

```html
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <title>Mitt första HTML-dokument</title>
</head>
<body>
    <h1>.SE är en oberoende allmännyttig organisation</h1>
    <p>.SE (Stiftelsen för Internetinfrastruktur) är en oberoende allmännyttig organisation som verkar för positiv utveckling av Internet i Sverige.</p>
    
    <h2>Internetutveckling</h2>
    <p>I enlighet med stiftelsens <a href="https://www.iis.se/om-se/urkund-och-stadgar">urkund och stadgar</a> gynnar vi på olika sätt Internetutvecklingen i Sverige. Det är inom denna verksamhet .SE investerar det överskott som genereras av registreringsavgifterna för domännamn.</p>
    
    <ul>
        <li>.SE ansvarar för Internets svenska toppdomän .se</li>
        <li>.SE ansvarar för administration och teknisk drift av det nationella domännamnregistret</li>
        <li>I enlighet med stiftelsens urkund och stadgar gynnar vi på olika sätt Internetutvecklingen i Sverige.</li>
    </ul>
</body>
</html>
```

#### Resultatet
<div style="border: 1px solid #CCC; border-radius: 5px; padding: 2em; margin-bottom:2em;">
<h1>.SE är en oberoende allmännyttig organisation</h1>
<p>.SE (Stiftelsen för Internetinfrastruktur) är en oberoende allmännyttig organisation som verkar för positiv utveckling av Internet i Sverige.</p>

<h2>Internetutveckling</h2>
<p>I enlighet med stiftelsens <a href="https://www.iis.se/om-se/urkund-och-stadgar">urkund och stadgar</a> gynnar vi på olika sätt Internetutvecklingen i Sverige. Det är inom denna verksamhet .SE investerar det överskott som genereras av registreringsavgifterna för domännamn.</p>

<ul>
    <li>.SE ansvarar för Internets svenska toppdomän .se</li>
    <li>.SE ansvarar för administration och teknisk drift av det nationella domännamnregistret</li>
    <li>I enlighet med stiftelsens urkund och stadgar gynnar vi på olika sätt Internetutvecklingen i Sverige.</li>
</ul>
</div>

### Två typer av taggar: inline och block
Alla taggar kan sorteras in i två grupper: inline och block. Innehåll som rubriker, listor och brödtext ska visas på en egen rad, och dessa taggar benämns som block-element. Namnet antyder att det fungerar som ett block på sidan och tar upp hela bredden, från sida till sida.

Taggen `<a>` som vi använder för att länka text är tydligt inte ett block-element. Där vill vi att det som kommer efter taggen ska lägga sig bredvid, och då är det ett inline-element. Du ser det tydligt i ditt exempel. Som tur är behöver du inte bry dig om det är inline eller block, det är satt automatiskt.

Så här hade ditt dokument sett ut om rubriker, brödtext och listor inte var block-element:

<div style="border: 1px solid #CCC; border-radius: 5px; padding: 2em; margin-bottom:2em;">
.SE är en oberoende allmännyttig organisation .SE (Stiftelsen för Internetinfrastruktur) är en oberoende allmännyttig organisation som verkar för positiv utveckling av Internet i Sverige. Internetutveckling I enlighet med stiftelsens urkund och stadgar gynnar vi på olika sätt Internetutvecklingen i Sverige. Det är inom denna verksamhet .SE investerar det överskott som genereras av registreringsavgifterna för domännamn. .SE ansvarar för Internets svenska toppdomän .se .SE ansvarar för administration och teknisk drift av det nationella domännamnregistret I enlighet med stiftelsens urkund och stadgar gynnar vi på olika sätt Internetutvecklingen i Sverige.
</div>

### Lägga in bilder

För att lägga in en bild använder vi taggen `<img>` som är förkortning av 'image', bild på svenska. Den kräver att vi har med två attribut: src (source) och alt (alternative).

Lägga in en bild:

```html
<img src="logo.png" alt=".SE logotyp">
```

![alt text](../.gitbook/assets/image-92.png)

I första attributet, src (källa), skriver du in bildens sökväg, namn och filändelse. I exemplet ovan ligger bilden logo.png i samma mapp som HTML-dokumentet. Du kan spara ned .SE logotyp som vi använder i exemplet.

I andra attributet, alt (alternativ), beskriver du bilden i text. Detta är för att sökmotorer, skärmläsare och andra "maskiner" ska kunna "se" bilden.

När bilden visas i webbläsaren är den i originalstorlek. Eventuell förminskning gör du i ett bildbehandlingsprogram.

#### Lägg in bilden i ditt dokument

```html
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <title>Mitt första HTML-dokument</title>
</head>
<body>
    <h1>.SE är en oberoende allmännyttig organisation</h1>
    <p><img src="logo.png" alt=".SE logotyp"> .SE (Stiftelsen för Internetinfrastruktur) är en oberoende allmännyttig organisation som verkar för positiv utveckling av Internet i Sverige.</p>
    
    <h2>Internetutveckling</h2>
    <p>I enlighet med stiftelsens <a href="https://www.iis.se/om-se/urkund-och-stadgar">urkund och stadgar</a> gynnar vi på olika sätt Internetutvecklingen i Sverige. Det är inom denna verksamhet .SE investerar det överskott som genereras av registreringsavgifterna för domännamn.</p>
    
    <ul>
        <li>.SE ansvarar för Internets svenska toppdomän .se</li>
        <li>.SE ansvarar för administration och teknisk drift av det nationella domännamnregistret</li>
        <li>I enlighet med stiftelsens urkund och stadgar gynnar vi på olika sätt Internetutvecklingen i Sverige.</li>
    </ul>
</body>
</html>
```

#### Resultatet

<div style="border: 1px solid #CCC; border-radius: 5px; padding: 2em; margin-bottom:2em;">
    <h1>.SE är en oberoende allmännyttig organisation</h1>
    <p><img src="../../images/image-96.png" alt=".SE logotyp"> .SE (Stiftelsen för Internetinfrastruktur) är en oberoende allmännyttig organisation som verkar för positiv utveckling av Internet i Sverige.</p>
    
    <h2>Internetutveckling</h2>
    <p>I enlighet med stiftelsens <a href="https://www.iis.se/om-se/urkund-och-stadgar">urkund och stadgar</a> gynnar vi på olika sätt Internetutvecklingen i Sverige. Det är inom denna verksamhet .SE investerar det överskott som genereras av registreringsavgifterna för domännamn.</p>
    
    <ul>
        <li>.SE ansvarar för Internets svenska toppdomän .se</li>
        <li>.SE ansvarar för administration och teknisk drift av det nationella domännamnregistret</li>
        <li>I enlighet med stiftelsens urkund och stadgar gynnar vi på olika sätt Internetutvecklingen i Sverige.</li>
    </ul>
</div>

#### Men vart ska den ligga?

Är du osäker på vart du ska lägga innehållet? Utgå från hur en tidning eller bok är utformad.

I HTML-koden ovan är bilden placerad inom ett textstycke (`<p>`), bilden hör alltså ihop med innehållet i det textstycket.

Det finns två enkla regler för vart du kan placera olika element:

1. Logik: Exempelvis har du troligen aldrig sett en lista i en rubrik.
2. Block-element kan inte läggas i inline-element, bara tvärtom.

!!! Failure

    ```html
    <p><h1>En rubrik</h1> Brödtext</p>
    ```

!!! Success

    ```html
    <h1>En rubrik</h1>
    <p>Brödtext</p>
    ```

!!! Failure

    ```html
    <p>Brödtext med <a href="http://iis.se/">länk</p></a>
    ```

!!! Success

    ```html
    <p>Brödtext med <a href="http://iis.se/">länk</a></p>
    ```

### Så fungerar mått

Bilden logo.png är 100 pixlar (px) bred och 43 pixlar hög. I tryck används centimeter, millimeter eller tum. För webbplatser finns fler mått än pixlar; em, pt och procent. Så vad är skillnaden? En enkel regel för textstorlek är 1em = 12pt = 16px = 100%. I exemplen håller vi oss till pixlar.

Exempel på bredder:

- 100 pixlar bred.
- 200 pixlar bred.
- 120 pixlar bred.
- 340 pixlar bred.
- 2 pixlar bred.
- 1 em bred.
- 5 em bred.
- 12 pt bred.
- 16 px bred.
- 50% (av 340 px) bred.

#### Bildexempel

100x90 avser 100 pixlar bred och 90 pixlar hög:

![alt text](../.gitbook/assets/image-92.png)

### Boxmodellen

Boxmodellen förklarar hur de olika ytorna kring ett blockelement fungerar. Som du säkert sett i ditt exempeldokument finns det marginal kring rubrikerna. För att ta ytterligare ett exempel: `<p>`, textstycke. Denna består av fyra ytor:

1. Margin (marginal) ger mellanrum mellan textstycket och andra element.
2. Border är en kantlinje man kan sätta runt ett element.
3. Padding ger utrymme mellan kantlinjen (border) och själva innehållet.
4. Innehållsytan där innehållet är placerat, texten i detta fall. Den har höjd och bredd.

Varje yta har fyra sidor: toppen, höger, botten och vänster.

![alt text](../.gitbook/assets/image-93.png)

### Standardvärden

Vissa taggar har redan från början marginaler, padding mm. De finns där för att sidan ska vara lättläslig utan något utseende (CSS).

Endast block-element nyttjar boxmodellen, inline-element som exempelvis `<a>` nyttjar inte boxmodellen.

Exempel på margin och padding:

- `<h1>` har margin som medföljande värde:

![alt text](../.gitbook/assets/image-94.png)

- `<ul>` har både margin och padding:

![alt text](../.gitbook/assets/image-95.png)

### Strukturera innehåll i boxar

Precis som i en tidning kan du sortera innehåll i **boxar** (block) som går att placera. Det har stora likheter med hur man arbetar med *Lego®-klossar*. För att skapa ett block används taggen `<div>`.

Exempel med div:

```html
<div>
    <h2>Internetutveckling</h2>
</div>
```

Ovan är ett block med en underrubrik. Här är tre div-element, varav en av dem ligger i en annan div. Strukturen är alltså följande:

1. Div 1
2. Div 2
3. Div 3

Exempel med div:

```html
<div>
    <h1>.SE är en oberoende allmännyttig organisation</h1>
    <p><img src="logo.png" alt=".SE logotyp"> .SE (Stiftelsen för Internetinfrastruktur) är en oberoende allmännyttig organisation som verkar för positiv utveckling av Internet i Sverige.</p>
</div>
<div>
    <h2>Internetutveckling</h2>
    <p>I enlighet med stiftelsens <a href="https://www.iis.se/om-se/urkund-och-stadgar">urkund och stadgar</a> gynnar vi på olika sätt Internetutvecklingen i Sverige. Det är inom denna verksamhet .SE investerar det överskott som genereras av registreringsavgifterna för domännamn.</p>

    <ul>
        <li>.SE ansvarar för Internets svenska toppdomän .se</li>
        <li>.SE ansvarar för administration och teknisk drift av det nationella domännamnregistret</li>
        <li>I enlighet med stiftelsens urkund och stadgar gynnar vi på olika sätt Internetutvecklingen i Sverige.</li>
    </ul>
</div>
```

### Semantiska taggar

Semantiska taggar ger HTML-strukturen mer meningsfulla element som beskriver innehållet på ett tydligare sätt för både utvecklare och sökmotorer. Här är några exempel:

- `<header>`: Innehåller introducerande eller navigerande innehåll.
- `<nav>`: Innehåller navigationslänkar.
- `<section>`: En sektion av innehåll som vanligtvis innehåller en rubrik.
- `<article>`: Självständigt, fristående innehåll.
- `<aside>`: Innehåll som är relaterat men inte en del av huvudflödet.
- `<footer>`: Innehåller sidfotsinnehåll, som författarinfo, juridisk information eller kontaktuppgifter.

Exempel på användning av semantiska taggar:

```html
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <title>Mitt första HTML-dokument</title>
</head>
<body>
    <header>
        <h1>.SE är en oberoende allmännyttig organisation</h1>
        <nav>
            <ul>
                <li><a href="#om-se">Om .SE</a></li>
                <li><a href="#internetutveckling">Internetutveckling</a></li>
                <li><a href="#kontakt">Kontakt</a></li>
            </ul>
        </nav>
    </header>
    <main>
        <article>
            <section>
                <h2>Om .SE</h2>
                <p>.SE (Stiftelsen för Internetinfrastruktur) är en oberoende allmännyttig organisation som verkar för positiv utveckling av Internet i Sverige.</p>
            </section>
            <section>
                <h2>Internetutveckling</h2>
                <p>I enlighet med stiftelsens <a href="https://www.iis.se/om-se/urkund-och-stadgar">urkund och stadgar</a> gynnar vi på olika sätt Internetutvecklingen i Sverige. Det är inom denna verksamhet .SE investerar det överskott som genereras av registreringsavgifterna för domännamn.</p>
            </section>
        </article>
        <aside>
            <h2>Relaterad information</h2>
            <p>Här kan du hitta mer information om .SE och vår verksamhet.</p>
        </aside>
    </main>
    <footer>
        <p>Kontaktinformation: <a href="mailto:info@iis.se">info@iis.se</a></p>
    </footer>
</body>
</html>
```

### Sammanfattning av HTML-kapitlet

- HTML är Språket för att markera innehåll.
- HTML-språket består av taggar för att markera olika typer av innehåll som brödtext, rubriker, länkar och listor.
- Taggar som markerar en sektion i innehållet öppnas före (`<h1>`) och stängs efteråt (`</h1>`), för att markera avslut.
- Taggar som inte markerar innehåll, exempelvis `<img>` som inkluderar en bild, har ingen stängning eftersom den inte markerar något innehåll.
- Attribut är egenskaper som kan sättas på elementet (`<a href="">`, `<img src="" alt="">`). För vissa taggar finns det krav på attribut.
- Vissa taggar får bara finnas inom en annan tagg, `<li>` är en sådan tagg som bara får finnas inom `<ul>` och `<ol>`.
- Det finns block- och inline-element.
- Du kan lägga in bilder med taggen `<img>`. Den kräver två attribut: src och alt.
- Grundläggande mått som pixlar (px).
- Logiken i boxmodellen och att några taggar redan har margin eller padding från början.
- Grunden med hur divar fungerar.

| Tagg        | Förkortning av | Attribut    | Inline/Block |
|-------------|----------------|-------------|--------------|
| `<h1>`      | Heading 1      |             | Block        |
| `<h2>`      | Heading 2      |             | Block        |
| `<p>`       | Paragraph      |             | Block        |
| `<a>`       | Anchor         | href        | Inline       |
| `<ol>`      | Ordered list   |             | Block        |
| `<ul>`      | Unordered list |             | Block        |
| `<li>`      | List item      |             | Block        |
| `<img>`     | Image          | src alt     | Inline       |
| `<div>`     | Division       |             | Block        |
| `<header>`  | Header         |             | Block        |
| `<nav>`     | Navigation     |             | Block        |
| `<section>` | Section        |             | Block        |
| `<article>` | Article        |             | Block        |
| `<aside>`   | Aside          |             | Block        |
| `<footer>`  | Footer         |             | Block        |

## Språk för utseende

### Introduktion till CSS (stilmall)

Till skillnad mot HTML som bara handlar om innehåll, är CSS enbart om utseende. Det innehåll vi markerat med hjälp av HTML kan vi nu ändra utseende för, exempelvis textstorlek för huvudrubriken `<h1>`.

CSS är till språket olikt HTML, istället för att markera innehåll kommer vi matcha och koppla ihop rätt utseende med rätt tagg (element). Tänk dig CSS som ett regelverk för hur saker och ting ska se ut, och precis som med HTML skriver du CSS-reglerna i ett vanligt textdokument och sparar med .css som filändelse.

Så här kan en CSS-regel se ut:

```css
h1 {
    font-size: 12px;
}
```

Den ändrar alla huvudrubrikers (`h1`) textstorlek till 12px.

I början av en CSS-regel anger vi vilka element som regeln ska gälla för, i det här fallet är det för huvudrubriken `h1`. Det följs av en "måsvinge" (krullparantes) `{` som har till funktion att markera starten för CSS-reglerna, och i slutet stängs den med `}`, vilket påminner om logiken i HTML om att stänga och öppna taggar.

Exemplet har en CSS-regel; `font-size`, som är en av många egenskaper i CSS, denna ändrar storleken på texten. Efter egenskapen `font-size` markeras avslutet på den med ett kolon, som är skiljetecken mellan egenskap och värde, 12px. Värdet avslutas med semikolon `;` som gör det möjligt att fylla på med fler CSS-egenskaper efteråt.

### Koppla ihop CSS med HTML

Vi återgår till ett tidigare exempel som bara innehåller en huvudrubrik, spara detta dokument som `index.html`, och spara CSS-dokumentet med den tidigare regeln (`h1 {font-size: 12px;}`) som `style.css`. Lägg `style.css` i samma katalog som `index.html`.

=== "index.html"

    ```html
    <!DOCTYPE html>
    <html lang="sv">
        <head>
            <meta charset="utf-8">
            <title>Mitt första HTML-dokument</title>
            <link rel="stylesheet" href="style.css">
        </head>
        <body>
            <h1>.SE är en oberoende allmännyttig organisation</h1>
        </body>
    </html>
    ```

=== "style.css"

    ```css
    h1 {
        font-size: 12px;
    }
    ```

Vi har lagt till en rad i HTML-dokumentet, `<link rel="stylesheet" href="style.css">`, som kopplar ihop `index.html` med `style.css`. Taggen `link` används för att bland annat länka in stilmallar. Attributet `rel` beskriver vad vi länkar in (stilmall) och `href`, som du stött på innan, visar sökvägen till filen `style.css`.

### Matcha CSS med rätt HTML-element

Nu när de två dokumenten är sammanlänkade kommer HTML-dokumentet hämta reglerna från CSS-dokumentet.

=== "index.html"

    ```html
    <!DOCTYPE html>
    <html lang="sv">
        <head>
            <meta charset="utf-8">
            <title>Mitt första HTML-dokument</title>
            <link rel="stylesheet" href="style.css">
        </head>
        <body>
            <h1>.SE är en oberoende allmännyttig organisation</h1>
        </body>
    </html>
    ```

=== "style.css"

    ```css
    h1 {
        font-size: 12px;
    }
    ```

Med hjälp av CSS-regeln har vi ändrat textstorleken från standard 32px till 12px. En huvudrubrik (`<h1>`) är från början 32px, det är ett värde den har med sig, precis som standard-marginalerna vi gick igenom tidigare i boxmodellen.

Regeln för `h1` kommer matcha alla `h1`-or i HTML-dokumentet och textstorleken på alla `h1`-or minskas till 12 pixlar.

### Färger

För att färgsätta innehåll och grafiska element, som text, länkar, linjer, bakgrunder mm. används ett hexadecimalt värde, RGB eller namn. Hex och RGB anger en kombination för (R)öd (G)rönt och (B)lått för att få rätt färg.

| Egenskap  | Värde             |
|-----------|-------------------|
| `color:`  | `#ffffff`         |
| `color:`  | `rgb(255,255,255)`|
| `color:`  | `white`           |

RGB kan också skrivas som procent, 255 = 100%. Alltså är `rgb(255,255,255)` samma som `rgb(100%,100%,100%)`.

Färgexempel

| Namn-värde | Hex-värde | RGB-värde           | Exempel                          |
|------------|-----------|---------------------|----------------------------------|
| black      | #000000   | rgb(0,0,0)          | `h1 {color: black;}`             |
| white      | #ffffff   | rgb(255,255,255)    | `h1 {color: #ffffff;}`           |
| red        | #ff0000   | rgb(255,0,0)        | `p {color: red;}`                |
| purple     | #800080   | rgb(128,0,128)      | `li {color: rgb(128,0,128);}`    |
| green      | #008000   | rgb(0,128,0)        | `h2 {color: green;}`             |
| lime       | #00ff00   | rgb(0,255,0)        | `a {color: #00ff00;}`            |
| yellow     | #ffff00   | rgb(255,255,0)      | `p {color: #ffff00;}`            |
| blue       | #0000ff   | rgb(0,0,255)        | `h1 {color: rgb(0,0,255);}`      |

CSS exempel:

```css
h1 {
    color: red;
}
```

```css
h1 {
    color: #ffff00;
}
```

```css
h1 {
    color: rgb(128,0,128);
}
```

```css
h1 {
    color: #00ff00;
}
```

### Flera regler och egenskaper

För att lägga till fler egenskaper till en och samma regel skriver du nästa egenskap direkt efter den första.

Flera egenskaper i samma regel:

```css
h1 {
    color: #00ff00;
    font-size: 12px;
}
```

Flera regler som matchar samma element:

```css
h1 {
    color: #00ff00;
}
h1 {
    font-size: 12px;
}
```

Det är ingen skillnad mellan de två exemplen ovan, resultatet blir det samma. Men alternativet med en regel och två egenskaper är att föredra då den är kortare och lättare att läsa.\
En naturlig process när man skriver CSS-regler är att ersätta värden, alltså "kollidera" egenskaper, där den sistnämnda alltid vinner.\
Det första exemplet till höger har två regler som matchar samma element, och egenskapen font-size kolliderar. Den sista regeln övervinner den första då den kommer sist:

```css
p {
    font-size: 32px;
    color: red;
}
p {
    font-size: 12px;
}
```

Exempel två har två regler, varav den första har två egenskaper som kolliderar. I den första regeln sätts först font-size till 32 pixlar, sedan till 10, och i sista regeln till 12:

```css
p {
    font-size: 32px;
    font-size: 10px;
}
p {
    font-size: 12px;
}
```

I exempel tre finns två stycken regler som matchar två olika element, <p> och h3. De har alltså inte med varandra att göra:

```css
h3 {
    font-size: 22px;
}
p {
    font-size: 12px;
}
```

!!! Tip

    Också i CSS kan du använda indentering (indrag) för att få en bra överblick.

### Ännu fler CSS-egenskaper

Du känner till `font-size` och `color`, här är några fler användbara.

=== "index.html"

    ```html
    <h1>.SE är en oberoende allmännyttig organisation</h1>
    <p><img src="logo.png" alt=".SE logotyp"> .SE (Stiftelsen för Internetinfrastruktur) är en oberoende allmännyttig organisation som verkar för positiv utveckling av Internet i Sverige.</p>
    <h2>Internetutveckling</h2>
    <p>I enlighet med stiftelsens <a href="https://www.iis.se/om-se/urkund-och-stadgar">urkund och stadgar</a> gynnar vi på olika sätt Internetutvecklingen i Sverige. Det är inom denna verksamhet .SE investerar det överskott som genereras av registreringsavgifterna för domännamn.</p>
    <ul>
        <li>.SE ansvarar för Internets svenska toppdomän .se</li>
        <li>.SE ansvarar för administration och teknisk drift av det nationella domännamnregistret</li>
        <li>I enlighet med stiftelsens urkund och stadgar gynnar vi på olika sätt Internetutvecklingen i Sverige.</li>
    </ul>
    ```

=== "style.css"

    ```css
    h1 {
        border-bottom: 1px solid #cccccc;
    }

    img {
        border: 1px solid green;
        padding: 10px;
        margin: 10px 0 10px 0;
    }

    h2 {
        color: #843232;
    }

    li {
        font-weight: bold;
    }
    ```

#### Resultatet

<div style="border: 1px solid #CCC; border-radius: 5px; padding: 2em; margin-bottom:2em;">
    <h1>.SE är en oberoende allmännyttig organisation</h1>
    <p><img src="../../images/image-96.png" alt=".SE logotyp"> .SE (Stiftelsen för Internetinfrastruktur) är en oberoende allmännyttig organisation som verkar för positiv utveckling av Internet i Sverige.</p>
    <h2>Internetutveckling</h2>
    <p>I enlighet med stiftelsens <a href="https://www.iis.se/om-se/urkund-och-stadgar">urkund och stadgar</a> gynnar vi på olika sätt Internetutvecklingen i Sverige. Det är inom denna verksamhet .SE investerar det överskott som genereras av registreringsavgifterna för domännamn.</p>
    <ul>
        <li>.SE ansvarar för Internets svenska toppdomän .se</li>
        <li>.SE ansvarar för administration och teknisk drift av det nationella domännamnregistret</li>
        <li>I enlighet med stiftelsens urkund och stadgar gynnar vi på olika sätt Internetutvecklingen i Sverige.</li>
    </ul>
</div>

!!! Tip

    Vi har gjort radbrytningar i de CSS-regler som har flera egenskaper, det gör koden mer lättläst.

I exemplet har vi ändrat huvudrubriken, bilden, underrubriken och punkterna i punktlistan.\
För dig är `border`, `padding`, `margin` och `font-weight` nya CSS-egenskaper.

### Margin, border, padding samt width och height

`margin`, `border`, `padding`, `width` och `height` fungerar enligt boxmodellen som vi tidigare gått igenom.

- `margin` förändrar vi den yttre marginalen kring elementet.
- `border` ändrar kantlinjen.
- `padding` förändrar den "inre" marginalen, innanför `border`.
- `width` ändrar bredden.
- `height` ändrar höjden.

![alt text](../.gitbook/assets/image-97.png)

#### Ändra flera värden

Eftersom det finns fyra sidor kan vi ändra alla fyra sidor till att ha olika värden. Därför kan vi ange fyra värden för margin, border och padding.

Du anger de efter varandra och de kommer läsas in klockvis, alltså första värdet är up (top), det andra höger (right), det tredje ned (bottom) och fjärde vänster (left)

`margin`:

```css
div {
    margin: 10px 20px 10px 30px;
}
```
![alt text](../.gitbook/assets/image-98.png)

`border`:

```css
div {
    border: 1px solid blue;
}
```
![alt text](../.gitbook/assets/image-99.png)

`padding`:

```css
div {
    padding: 15px 20px 10px 10px;
}
```
![alt text](../.gitbook/assets/image-100.png)

`width`:

```css
div {
    width: 200px;
}
```
![alt text](../.gitbook/assets/image-101.png)

`height`:

```css
div {
    height: 90px;
}
```
![alt text](../.gitbook/assets/image-102.png)

!!! Tip

    En `div` är alltid osynlig, höjden bestäms utifrån höjden av innehållet. Bredden är 100%, om du inte använder `width`-egenskapen.

### Andra former av CSS-regler

Förutom att använda taggar som **selektorer** kan du använda andra typer av **selektorer** för att välja element i HTML-dokumentet.

#### Klasser

Med en klass kan du skapa ett *namn* som du kan använda för att välja element i HTML-dokumentet. Klasser används ofta för att gruppera **flera element** som har gemensamma egenskaper.
En klass definieras med en punkt `.` före namnet.

Exempel på användning av klasser:

=== "index.html"

    ```html
    <h1 class="viktig">.SE är en oberoende allmännyttig organisation</h1>
    <p class="viktig">.SE (Stiftelsen för Internetinfrastruktur) är en oberoende allmännyttig organisation som verkar för positiv utveckling av Internet i Sverige.</p>
    ```
=== "style.css"

    ```css
    .viktig {
        font-weight: bold;
        color: red;
    }
    ```

I exemplet ovan har vi skapat en klass `viktig` som används för att markera viktig text. Klassen har två egenskaper: `font-weight` och `color`.

### Enheter: em och rem

Förutom pixlar (px) finns det andra måttenheter som används inom webbdesign, till exempel `em` och `rem`. Dessa enheter är särskilt användbara för att skapa responsiva webbplatser där text och layout skalas proportionellt.

#### em

`em` är en relativ måttenhet som baseras på teckenstorleken för det aktuella elementet. Om teckenstorleken på en paragraf är 16px, då är 1em lika med 16px. Om du ställer in ett element till 2em blir det två gånger större än teckenstorleken för det elementet.

=== "index.html"

    ```html
    <p>Detta är normal text.</p>
    <p class="stor-text">Detta är större text.</p>
    ```

=== "style.css"

    ```css
    p {
        font-size: 16px;
    }
    .stor-text {
        font-size: 2em; /* 32px, eftersom 1em = 16px */
    }
    ```

#### rem

`rem` står för "root em" och är baserat på teckenstorleken för rot-elementet (`<html>`). Om rotens teckenstorlek är 16px, då är 1rem lika med 16px oavsett var du använder det i dokumentet.

=== "index.html"

    ```html
    <p>Detta är normal text.</p>
    <p class="stor-text">Detta är större text.</p>
    ```
=== "style.css"

    ```css
    html {
        font-size: 16px;
    }
    .stor-text {
        font-size: 2em; /* 32px, eftersom 1rem = 16px */
    }
    ```

#### Skillnaden mellan em och rem

- `em` är relativ till teckenstorleken för det aktuella elementet.
- `rem` är relativ till teckenstorleken för rot-elementet (`<html>`).

Använd `rem` när du vill att teckenstorleken ska vara konsekvent över hela dokumentet, och använd `em` när du vill att storleken ska vara relativ till det aktuella elementet.

Genom att förstå och använda semantiska taggar samt enheter som `em` och `rem`, kan du skapa mer strukturerade och responsiva webbplatser.

### CSS Grid

CSS Grid är en kraftfull layoutmetod för att skapa komplexa och responsiva webblayouts. Med grid kan du enkelt placera element i ett rutnät och kontrollera deras storlek och position.

Exempel på användning av CSS Grid för att två kolumner:

=== "index.html"

    ```html
    <!DOCTYPE html>
    <html lang="sv">
    <head>
        <meta charset="UTF-8">
        <title>CSS Grid Exempel</title>
        <link rel="stylesheet" href="style.css">
    </head>
    <body>
        <div class="grid-kontainer">
            <div class="kolumn">Kolumn 1</div>
            <div class="kolumn">Kolumn 2</div>
        </div>
    </body>
    </html>
    ```

=== "style.css"

    ```css
    .grid-kontainer {
        display: grid;
        grid-template-columns: auto auto;
        gap: 10px;
    }
    .kolumn {
        background-color: #f2f2f2;
        padding: 20px;
        border: 1px solid #ccc;
        border-radius: 5px;
    }
    ```

#### Resultatet

<div style="border: 1px solid #CCC; border-radius: 5px; padding: 2em; margin-bottom:2em;">
    <style>
        .grid-kontainer {
            display: grid;
            grid-template-columns: auto auto;
            gap: 10px;
        }
        .kolumn {
            background-color: #f2f2f2;
            padding: 20px;
            border: 1px solid #ccc;
            border-radius: 5px;
        }
    </style>
    <div class="grid-kontainer">
        <div class="kolumn">lorem ipsum dolor sit amet</div>
        <div class="kolumn">lorems ipsum dolor sit amet</div>
    </div>
</div>

I detta exempel har vi en grid-container som innehåller sex grid-items. Vi har använt `grid-template-columns` för att definiera tre kolumner med automatisk bredd och `gap` för att ange avståndet mellan rutorna.

### Responsivitet med @media

Med `@media`-regler kan du göra din webbplats responsiv, vilket innebär att den anpassar sig till olika skärmstorlekar.

Exempel på `@media`-användning:

=== "index.html"

    ```html
    <!DOCTYPE html>
    <html lang="sv">
    <head>
        <meta charset="UTF-8">
        <title>Responsivitet Exempel</title>
        <link rel="stylesheet" href="style.css">
    </head>
    <body>
        <div class="kontainer">
            <h1>Responsiv webbdesign</h1>
            <p>Denna webbplats anpassar sig efter skärmstorleken.</p>
        </div>
    </body>
    </html>
    ```

=== "style.css"

    ```css
    body {
        font-family: Arial, sans-serif;
    }

    .kontainer {
        width: 80%;
        margin: 0 auto;
        background-color: #f2f2f2;
        padding: 20px;
    }

    /* För skärmstorlekar mindre än 768px */
    @media (max-width: 768px) {
        .kontainer {
            width: 100%;
            padding: 10px;
        }
        h1 {
            font-size: 18px;
        }
    }

    /* För skärmstorlekar mindre än 480px */
    @media (max-width: 480px) {
        h1 {
            font-size: 16px;
        }
    }
    ```

I detta exempel har vi använt två `@media`-regler. Den första ändrar bredden på `.container` och textstorleken på `h1` när skärmstorleken är mindre än 768px. Den andra regeln ändrar textstorleken ytterligare när skärmstorleken är mindre än 480px.

### Sammanfattning av CSS-kapitlet

- CSS är språket för att designa innehåll.
- Hur CSS-regler är uppbyggda.
- Koppla ihop HTML-dokumentet och CSS-filen med HTML `link`-taggen.
- Matcha CSS-regel med rätt element.
- Färgsättning med bland annat hex-värde (#ffffff).
- Hur det fungerar när CSS-regler "tar ut" varandra.
- Vi gick igenom egenskaper som `font-size` och `color`.
- Återgick till boxmodellen och använde `margin`, `border`, `padding` mm.
- Introduktion till CSS Grid för att skapa flexibla layouter.
- Användning av @media-regler för att skapa responsiva webbplatser.

| Egenskap      | Värde-exempel               | Förklaring                                      | Exempel                               |
|---------------|-----------------------------|------------------------------------------------|---------------------------------------|
| `font-size`   | 12px                        | Textstorlek                                    | `h1 {font-size: 12px}`                |
| `color`       | #ffffff                     | Textfärg                                       | `p {color: #000000}`                  |
| `border`      | 1px solid #000000           | Kantlinje runt hela elementet                  | `h2 {border: 1px solid #000000;}`     |
| `padding`     | 20px 10px 20px 15px         | Marginal innanför kantlinjen                   | `h1 {padding: 12px}`                  |
| `margin`      | 40px                        | Marginal utanför kantlinjen                    | `h1 {margin: 20px}`                   |
| `font-weight` | bold/normal                 | Gör text fetstilt eller tunnare                | `p {font-weight: bold}`               |
| `display`     | grid                        | Skapar en grid-layout                          | `.grid-container {display: grid;}`    |
| `grid-template-columns` | auto auto auto   | Definierar kolumner i en grid-layout            | `.grid-container {grid-template-columns: auto auto auto;}` |
| `@media`      | (max-width: 768px)          | Används för att applicera CSS-regler vid specifika skärmstorlekar | `@media (max-width: 768px) {...}`     |

Genom att använda CSS kan du skapa snygga och responsiva webblayouts som fungerar på alla enheter.

 > Referens: [https://pragmatisk.se/](https://pragmatisk.se/)