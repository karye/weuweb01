---
description: Använda <div> för att skapa en snygg ruta
---

# Receptsida för scones

Här är en annan övning där vi använder oss av en `div`-box för att skapa en **kontainer**. Hela receptet ligger i `div`-boxen. `div`-boxen stylas med gul bakgrundsfärg och med en ram.

## Resultat

![Scones sida](../.gitbook/assets/image-114.png)

## Video

### Egna fonter på hemsidan med Google fonts

{% embed url="https://youtu.be/A-lkKqwOrsw?si=zIz6zr_oZ3Fq3QgP" %}

### Så funkar Box-sizing i CSS

{% embed url="https://youtu.be/vxecgfVmBB8?si=Q8C7HxQsUSaPC5nQ" %}

## Steg 1 - förberedelser - webbrot

* Skapa en mapp som heter **scones**
* Skapa en webbsida som heter **scones.html**
* Skapa en CSS-fil som heter **style.css**
* Skapa en mapp **bilder**

## Steg 2 - skapa HTML-sidan

### HTML-strukturen

Såhär ser sidans HTML-struktur ut:

* `div` - en box 
  * `h1` - en rubrik
  * `p` - en beskrivning av receptet
  * `h2` - en mellanrubrik
  * `ul` - en lista med ingredienser
    * `li` - en ingrediens
    * ... fler ingredienser
  * `h2` - en mellanrubrik
  * `p` - "gör så här" beskrivning
  * ... fler stycken

### Grundkoden

* Börja med grundkoden
* Fyll i alla HTML-element som bygger upp sidan

### Scones receptet

* Välj ett recept på [http://www.scones.se](http://www.scones.se/scones-med-apple-och-kanel)

![](<../.gitbook/assets/image (102).png>)

## Steg 3 - snygga till sidan med CSS

### CSS-reglerna

* Infoga först CSS-reset
* Infoga alla CSS-regler som motsvarar de taggar vi använder
* Skapa en ruta med `div`-elementet genom att låsa bredd och höjd

```css	
/* Enkel CSS-reset */
html {
    box-sizing: border-box;
}
*, *:before, *:after {
    box-sizing: inherit;
}
body, h1, h2, h3, h4, h5, h6, p, ul {
    margin: 0;
    padding: 0;
}
img {
    max-width: 100%;
    height: auto;
}
```

#### CSS-reset

För att underlätta styling av sidan kan vi använda en CSS-reset. Det är en samling CSS-regler som återställer alla element till samma utgångsläge. Det gör att vi slipper tänka på att olika webbläsare har olika utgångsläge.

```css
/* Enkel CSS-reset */
html {
    box-sizing: border-box;
}
*, *:before, *:after {
    box-sizing: inherit;
}
body, h1, h2, h3, h4, h5, h6, p, ul {
    margin: 0;
    padding: 0;
}
img {
    max-width: 100%;
    height: auto;
}
```

### Välj typsnitt

* Gå till [Google Fonts](https://fonts.google.com) och välj tre typsnitt
  * Ett typsnitt för `<h1>` och `<h2>`
  * Ett typsnitt för `<p>`
  * Ett typsnitt för `<ul>`
* Ställ in texternas storlek med `font-size`

### Ställ in bakgrundsbilden

* Välj ut ett foto på [unsplash.com](https://unsplash.com)
* Ladda ned fotot i mappen **bilder**
* Använd `background-image` på `body`-elementet
* Använd `margin` på elementen

![](<../.gitbook/assets/image (103).png>)


