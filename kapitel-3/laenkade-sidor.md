---
description: Hur man enkelt skapar flera sidor med samma grundsstilar.
---

# Länkade sidor

När man skapar flera sidor, kan man behöva länka mellan sidorna, och/eller länka till bilder. Det kallas för sökvägar. Här är en övning där vi använder oss av sökvägar för att länka mellan sidorna.

Det finns två typer av sökvägar:

* **Relativa sökvägar** - länkar till filer i samma mapp
* **Absoluta sökvägar** - länkar till filer från en root-mapp

Vi använder oss också av en klasser för att styla sidornas bakgrundsbilder olika. Första **arbete1.html** får en bakgrundsbild, och andra **arbete2.html** får en annan bakgrundsbild.

## Resultat

![arbete1.html](<../.gitbook/assets/image (55).png>)
![arbete2.html](<../.gitbook/assets/image (56).png>)

## Video

### 23: File Paths In HTML and CSS | Learn HTML and CSS | Full Course For Beginners

{% embed url="https://youtu.be/EJ0xvY5wT5Q?si=kRUA7G5MFkI1-WhS" %}

### CSS Classer och styletaggen - HTML och CSS nybörjarguide del 5

{% embed url="https://youtu.be/VHa9bFFPtc8?si=Mh_UB4Zhk6R6Sk6r" %}

## Steg 1 - förberedelser - webbrot

* Skapa en mapp som heter **arbete**
* Skapa en webbsida som heter **arbete1.html**
* Skapa en CSS-fil som heter **style.css**
* Skapa en mapp **bilder**

## Steg 2 - skapa HTML-sidan

{% hint style="warning" %}
Viktigt! Skapa _bara_ **arbete1.html**\
De andra sidorna skapas senare
{% endhint %}

### Dikten av Ulf Lundell

```
Arbete av Ulf Lundell

Om jag inte arbetade
vad i helvete skulle jag
göra då?

Knarka kanske
Men det är ju också ett jobb
Ett jävla flängande med
inbrott och själva köpandet av
skiten och sen om igen
så det utesluter vi

Jag kunde leva på andra
men risken finns ju
att också det är ett rätt så hårt
arbete

Jag kunde lägga in mig på
sjukhus och bara ligga där
blickstilla
men jag kan ge mej fan på
att det också med tiden skulle kännas som ett
arbete

Så jag arbetar väl på då
Jag har väl inget val

Men någonting säger mej
att Gud från början
inte alls menade att vi skulle
syssla med nåt satans
arbete
```

### HTML-strukturen

Såhär ser sidans HTML-struktur ut:

* `div` - en box 
  * `h1` - en rubrik
  * `p` - en vers av dikten
  * `p` 
    * `a` - en länk till nästa sida

### Grundkoden till arbete1.html

* Börja med grundkoden
* Fyll i alla HTML-element som bygger upp sidan
* Välj ett tecken som ser ut som en [pil-åt-höger](https://dev.w3.org/html5/html-author/charref)
* Länka pil-åt-höger till sidan efter: **arbete2.html**
* Vi ger också `<body>`-elementet klassen `.sida1` på följande sätt:

```html
<body class="sida1">
...
</body>
```

![](<../.gitbook/assets/image (62).png>)

## Steg 3 - snygga till sidan med CSS

### CSS-reglerna

* Infoga överst CSS-reset
* Infoga alla CSS-regler som motsvarar de taggar vi använder
* Skapa en ruta med `div`-elementet genom att låsa bredd och höjd

```css	
/* Enkel CSS-reset */
html {
    box-sizing: border-box;
}
*, *:before, *:after {
    box-sizing: inherit;
}
body, h1, h2, h3, h4, h5, h6, p, ul {
    margin: 0;
    padding: 0;
}
img {
    max-width: 100%;
    height: auto;
}
```

### Välj typsnitt

* Gå till [Google Fonts](https://fonts.google.com) och välj två typsnitt
  * Ett typsnitt för `<h1>` och `<h2>`
  * Ett typsnitt för `<p>`
* Ställ in texternas storlek med `font-size`

### Ställ in bakgrundsbilden på första sidan med klassen .sida1

* Välj ut ett mönster som kan repeteras från [https://pixabay.com](https://pixabay.com/images/search/seamless%20pattern/)
* Ladda ned 6 mönster-bilder i mappen **bilder**
* Använd `background-image` på `body`-elementet med klassen `.sida1`
* Använd `padding`, `margin` och `border` på elementen

### Styla div-boxen

* Ställ in bredden med `width: 600px`
* Ställ in `margin` och `padding` på div-boxen
* Välj en kant enligt [border-style](https://developer.mozilla.org/en-US/docs/Web/CSS/border-style)

![](../.gitbook/assets/image-52.png)

## Nästa HTML-sida

{% hint style="important" %}
1. Spara **arbete1.html**
2. I menyn **File **välj **Save as...**
3. Spara nu en ny kopia **arbete2.html**
{% endhint %}

### HTML-strukturen

Såhär ser sidans HTML-struktur ut:

* `div` - en box 
  * `p` 
    * `a` - en länk till föregående sida
  * `h1` - en rubrik
  * `p` - en vers av dikten
  * `p` 
    * `a` - en länk till nästa sida

### Ställ in en ny bakgrund med klassen .sida2

* Välj ut ett till mönster som kan repeteras från [https://www.freepik.com/free-photos-vectors/seamless-pattern](https://www.freepik.com/free-photos-vectors/seamless-pattern)
* Använd `background-image` på `body`-elementet med klassen `.sida2`
* Länka pil-framåt tillbaka till sidan före: **arbete1.html**
* Länka pil-bakåt till sidan efter: **arbete3.html**

![](../.gitbook/assets/image-53.png)