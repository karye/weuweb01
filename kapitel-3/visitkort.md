---
description: Träna på att använda div-boxen för att skapa en kontainer
---

# Mitt visitkort

Den här övningen går ut på att skapa ett visitkort. Vi använder oss av en `div`-box för att skapa en **kontainer**. En `div`-box blir som en behållare som vi kan stoppa andra element i.

Sen kan vi styla `div`-boxen med `width`, `margin` och `padding` för att få en snygg kontainer.

## Resultat

![](../.gitbook/assets/image-48.png)

## Video 

### Egna fonter på hemsidan med Google fonts

{% embed url="https://youtu.be/A-lkKqwOrsw?si=zIz6zr_oZ3Fq3QgP" %}

### Så funkar Box-sizing i CSS

{% embed url="https://youtu.be/vxecgfVmBB8?si=Q8C7HxQsUSaPC5nQ" %}

## Steg 1 - förberedelser - webbrot

* Skapa en mapp som heter **visitkort**
* Skapa en webbsida som heter **index.html**
* Skapa en CSS-fil som heter **style.css**
* Skapa en mapp **bilder**

## Steg 2 - skapa HTML-sidan

### HTML-strukturen

Såhär ser sidans HTML-struktur ut:

* `div` - en box för visitkortet
  * `h1` - en rubrik
  * `p` - en beskrivning
  * `img` - en bild

### Grundkoden

* Börja med grundkoden
* Fyll i alla HTML-element som bygger upp sidan

![](../.gitbook/assets/image-49.png)

## Steg 3 - snygga till sidan med CSS

### CSS-reglerna

* Infoga alla CSS-regler som motsvarar de taggar vi använder
* Skapa en ruta med `div`-elementet genom att låsa bredd och höjd

![](<../.gitbook/assets/image (51).png>)

### Välj två typsnitt

* Gå till [Google Fonts](https://fonts.google.com) och välj två typsnitt
  * Ett typsnitt för `<h1>` och `<h2>`
  * Ett typsnitt för `<p>`
* Ställ in texternas storlek med `font-size`

### Ställ in bakgrundsbilden

* Välj ut ett foto på [unsplash.com](https://unsplash.com)
* Ladda ned fotot i mappen **bilder**
* Anpassa fotot storlek till ca 2000px bredd i [https://pixlr.com/se/x/](https://pixlr.com/se/x/)
* Använd `background-image` på `body`-elementet

### Anpassa mellanrum

* Använd `margin` på elementen

### Infoga en CSS-reset

För att underlätta styling av sidan kan vi använda en CSS-reset. Det är en samling CSS-regler som återställer alla element till samma utgångsläge. Det gör att vi slipper tänka på att olika webbläsare har olika utgångsläge.

```css
/* Enkel CSS-reset */
html {
    box-sizing: border-box;
}
*, *:before, *:after {
    box-sizing: inherit;
}
body, h1, h2, h3, h4, h5, h6, p, ul {
    margin: 0;
    padding: 0;
}
img {
    max-width: 100%;
    height: auto;
}
```

![](../.gitbook/assets/image-50.png)

### Färdigställ sidan

* Prova att ändra färger, storlekar och placeringar
* Använd `border-radius` för att göra hörnen runda
* Använd `box-shadow` för att ge en skuggeffekt
