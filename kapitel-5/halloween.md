# Skapa en Halloween-webbplats 🎃

I den här uppgiften ska du skapa en egen webbplats med Halloween-tema! Du ska göra tre webbsidor som handlar om pumpor, spöken och vampyrer. Alla sidor ska vara sammankopplade med en meny så att man kan klicka mellan dem.

## Krav

* Tre sidor: Pumpor, Spöken och Vampyrer
* En meny som låter dig navigera mellan sidorna
* En bild och text på varje sida som beskriver temat
* En snygg design med CSS som ger en Halloween-känsla

## Vad du ska göra

1. **Skapa en meny** där du har länkar till de tre sidorna: Pumpor, Spöken och Vampyrer.
2. **Bygg tre webbsidor** – en för varje tema – som innehåller en bild, en rubrik och en kort text som beskriver ämnet.
3. **Använd CSS** för att göra sidorna snygga och ge dem ett Halloween-tema med färger som orange, svart och lila.

## Steg-för-steg-guide

### Steg 1 - förberedelser

* Skapa en mapp på din dator som heter **halloween-webbplats**.
* Skapa tre HTML-filer i mappen och döp dem till tex **pumpor.html**, **spöken.html**, **vampyrer.html**.
* Skapa en CSS-fil som heter **style.css**.
* Skapa en mapp i din halloween-webbplats som heter **bilder** där du lägger bilder på pumpor, spöken och vampyrer.

### Steg 2 - skapa dina HTML-sidor

På varje sida ska du ha följande:

* En rubrik (h1) med temat för sidan, t.ex. "Pumpor".
* En meny som man kan klicka på för att byta mellan sidorna.
* En bild (img) som passar temat, t.ex. en pumpa på sidan om pumpor.
* En kort text (p) som beskriver temat på sidan.

### Steg 3 - designa sidan med CSS

* Lägg till en läskig font från [Google Fonts](https://fonts.google.com) och experimentera med olika bakgrundsbilder!
* Börja med att lägga till en CSS-reset så att alla webbläsare startar med samma grundinställningar:
    ```css
    html {
        box-sizing: border-box;
    }
    *, *:before, *:after {
        box-sizing: inherit;
    }
    body, h1, p, ul {
        margin: 0;
        padding: 0;
    }
    ```
* Lägg till en färgpalett som ger en Halloween-känsla! Se tjänster som [Coolors](https://coolors.co) för inspiration.
