---
description: Hur man bygg en hel webbplats med meny
---

# Webbplatsen 5 artister

I den här övningen ska vi bygga en webbplats med fem webbsidor. Varje sida ska handla om en artist. Vi använder oss av en meny för att navigera mellan sidorna. 

Menyn byggs med en lista `ul` och varje knapp med `li`. Menyknapparna läggs horisontellt med `display: flex` och knapparna får en bakgrundsfärg och en `hover`-färg.

Vi använder oss också av HTML `figure` och `figcaption` för att skapa en bild med tillhörande bildtext.

## Resultat

![](<../.gitbook/assets/image (66).png>)

## Video

### Så funkar CSS Flexbox

{% embed url="https://youtu.be/waVu2pJ2PUU?si=2vsw8YVMvadc9pr4" %}

### Så funkar Box-sizing i CSS

{% embed url="https://youtu.be/vxecgfVmBB8?si=Q8C7HxQsUSaPC5nQ" %}

### Meny med lista

Ett vanligt sätt att bygga en meny byggs är med en lista `ul` och varje knapp med `li`.

```html
<ul>
    <li><a href="index.html">Hem</a></li>
    <li><a href="om.html">Om</a></li>
    <li><a href="kontakt.html">Kontakt</a></li>
</ul>
```

## Steg 1 - förberedelser - webbrot

* Skapa en mapp som heter **webbplats-meny**
* Skapa en webbsida som heter **sida1.html**
* Skapa en CSS-fil som heter **style.css**
* Skapa en mapp **bilder**

## Steg 2 - skapa HTML-sidan

### HTML-strukturen

Såhär ser sidans HTML-struktur ut:

* `.kontainer` - en stor box för hela sidan
  * `h1` - en rubrik
  * `ul` - en lista med menyval
    * `li` - ett menyval
  * `h2` - en mellanrubrik
  * `figure` - en box för en bild
    * `img` - en bild
    * `figcaption` - en bildtext
  * `p` - en beskrivning
  * ...
  * `p.sidfot` - en sidfot

### Grundkoden

* Börja med grundkoden
* Fyll i alla HTML-element som bygger upp sidan

![](<../.gitbook/assets/image (75).png>)

## Steg 3 - styla till sidan med CSS

### CSS-reglerna

* Infoga först CSS-reset
* Infoga alla CSS-regler som motsvarar de taggar vi använder
* Gå till [Google Fonts](https://fonts.google.com) och välj två typsnitt

```css	
/* Enkel CSS-reset */
html {
    box-sizing: border-box;
}
*, *:before, *:after {
    box-sizing: inherit;
}
body, h1, h2, h3, h4, h5, h6, p, ul {
    margin: 0;
    padding: 0;
}
img {
    max-width: 100%;
    height: auto;
}
```

![](<../.gitbook/assets/image (76).png>)

### Välj färgpalett

* Välj färgpalett på [coolors.co](https://coolors.co)
* Prova skapa färgpalett från bild eller en färdig palett
* Exportera färgerna och kopiera

![](<../.gitbook/assets/image (85).png>)

* Klistra in färgerna i **CSS-filen**
* Använd färgerna i CSS-reglerna

![](../.gitbook/assets/image-60.png)

### Styla menyn

* Styla bakgrundsfärgern på knapparna
* Styla text i knapparna
* Styla `hover` färgen på knapparna
* Ställ in sidan som är aktiv i menyn

![](../.gitbook/assets/image-59.png)